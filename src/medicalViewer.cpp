/***********************************************************************
medicalViewer - Vrui application to test new mesh classes.
Copyright (c) 2009-2013 Oliver Kreylos
***********************************************************************/

#include "medicalViewer.h"

#include <assert.h>
#include <string.h>
#include <iostream>
#include <Misc/FunctionCalls.h>
#include <Misc/ThrowStdErr.h>
#include <Misc/FileNameExtensions.h>
#include <Misc/File.h>
#include <Geometry/Plane.h>
#include <Geometry/LinearUnit.h>
#include <Geometry/OutputOperators.h>
#include <GL/gl.h>
#include <GL/GLVertexTemplates.h>
#include <GL/GLMaterial.h>
#include <GL/GLTransformationWrappers.h>
#include <GLMotif/WidgetManager.h>
#include <GLMotif/PopupWindow.h>
#include <GLMotif/RowColumn.h>
#include <GLMotif/Menu.h>
#include <GLMotif/PopupMenu.h>
#include <GLMotif/Margin.h>
#include <GLMotif/Label.h>
#include <GLMotif/Button.h>
#include <GLMotif/TextField.h>
#include <Vrui/Vrui.h>
#include <Vrui/InputDevice.h>
#include <Vrui/InputGraphManager.h>
#include <Vrui/CoordinateManager.h>
#include <Vrui/Viewer.h>
#include <Vrui/ToolManager.h>
#include <Vrui/DisplayState.h>

#include "MaterialManager.h"
#include "PolygonModel.h"
#include "HierarchicalTriangleSetBase.h"
#include "MultiModel.h"
#include "ReadPLYFile.h"
#include "ReadLWOFile.h"
#include "ReadLWSFile.h"
#include "ReadASEFile.h"
#include "ReadOBJFileWithMesh.h"

/********************************************************
Static elements of class medicalViewer::ModelProbeTool:
********************************************************/

medicalViewer::ModelProbeToolFactory* medicalViewer::ModelProbeTool::factory=0;

/************************************************
Methods of class medicalViewer::ModelProbeTool:
************************************************/

medicalViewer::ModelProbeTool::ModelProbeTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment)
	:Vrui::Tool(factory,inputAssignment),
	 active(false),
	 haveIntersection(false)
	{
	}

void medicalViewer::ModelProbeTool::buttonCallback(int,Vrui::InputDevice::ButtonCallbackData* cbData)
	{
	active=cbData->newButtonState;

	if(active)
		{
		/* Get pointer to input device: */
		Vrui::InputDevice* iDevice=getButtonDevice(0);

		/* Calculate ray equation in navigation coordinates: */
		Vrui::Point p0=iDevice->getPosition();
		Vrui::Vector dir=iDevice->getRayDirection();
		Vrui::Point p1=p0+dir*Vrui::getBackplaneDist();
		PolygonModel::Point mp0=Vrui::getInverseNavigationTransformation().transform(p0);
		PolygonModel::Point mp1=Vrui::getInverseNavigationTransformation().transform(p1);
		
		/* Check if the model is a hierarchical triangle set or contains one: */
		const HierarchicalTriangleSetBase* hts=dynamic_cast<HierarchicalTriangleSetBase*>(application->model);
		if(hts==0)
			{
			MultiModel* mm=dynamic_cast<MultiModel*>(application->model);
			if(mm!=0)
				hts=mm->getHierarchicalTriangleSet();
			}
		if(hts!=0)
			{
			/* Find the submesh intersected by the ray: */
			std::cout<<"Searching submesh"<<std::endl;
			application->subMesh=hts->findSubMesh(mp0,mp1);
			// MODIFIED HERE: Added the two following code lines
			HierarchicalTriangleSetBase* htsa=dynamic_cast<HierarchicalTriangleSetBase*>(application->model);
			if(application->subMesh)
			{
				application->mutSubMesh = htsa->findSubMeshByIndex(application->subMesh->getMeshIndex());
				application->selectedMesh = application->subMesh->getMeshIndex() - 1;
			}
			}
		else
		{
			application->subMesh = 0;
			application->selectedMesh = -1;
		}

		/* Update the application's submesh data dialog: */
		application->updateSubMeshDialog();
		}
	}

void medicalViewer::ModelProbeTool::frame(void)
	{
	if(active)
		{
		/* Get pointer to input device: */
		Vrui::InputDevice* iDevice=getButtonDevice(0);
		
		/* Calculate ray equation in navigation coordinates: */
		Vrui::Point p0=iDevice->getPosition();
		Vrui::Vector dir=iDevice->getRayDirection();
		Vrui::Point p1=p0+dir*Vrui::getBackplaneDist();
		PolygonModel::Point mp0=Vrui::getInverseNavigationTransformation().transform(p0);
		PolygonModel::Point mp1=Vrui::getInverseNavigationTransformation().transform(p1);
		
		/* Probe the shit out of the MODEL!: */
		PolygonModel::Point mp=application->model->intersect(mp0,mp1);
		if(mp!=mp1)
			{
			haveIntersection=true;
			intersection=mp;
			}
		else
			haveIntersection=false;
		}
	}

void medicalViewer::ModelProbeTool::display(GLContextData& contextData) const
	{
	const Vrui::DisplayState& displayState=Vrui::getDisplayState(contextData);

	if(haveIntersection)
		{
		glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
		glDisable(GL_LIGHTING);
		glLineWidth(3.0f);
		
		/* Go to navigational coordinates: */
		glPushMatrix();
		glLoadIdentity();
		glMultMatrix(displayState.modelviewNavigational);
		
		Vrui::Scalar s=Vrui::getInverseNavigationTransformation().getScaling()*Vrui::getUiSize()*Vrui::Scalar(4);
		glColor3f(1.0f,0.0f,1.0f);
		glBegin(GL_LINES);
		glVertex(intersection[0]-s,intersection[1],intersection[2]);
		glVertex(intersection[0]+s,intersection[1],intersection[2]);
		glVertex(intersection[0],intersection[1]-s,intersection[2]);
		glVertex(intersection[0],intersection[1]+s,intersection[2]);
		glVertex(intersection[0],intersection[1],intersection[2]-s);
		glVertex(intersection[0],intersection[1],intersection[2]+s);
		glEnd();
		
		glPopMatrix();
		glPopAttrib();
		}
	}

/********************************************************
Static elements of class medicalViewer::RotationTool:
********************************************************/

medicalViewer::RotationToolFactory* medicalViewer::RotationTool::factory=0;

/************************************************
Methods of class medicalViewer::RotationTool:
************************************************/

medicalViewer::RotationTool::RotationTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment)
	:Vrui::Tool(factory,inputAssignment)
	{
	}

void medicalViewer::RotationTool::buttonCallback(int,Vrui::InputDevice::ButtonCallbackData* cbData)
	{
	application->rotateModels = cbData->newButtonState;
	}

/********************************************************
Static elements of class medicalViewer::ChangeSliceTool:
********************************************************/

medicalViewer::ChangeSliceToolFactory* medicalViewer::ChangeSliceTool::factory=0;

/************************************************
Methods of class medicalViewer::MoveSliceTool:
************************************************/

medicalViewer::ChangeSliceTool::ChangeSliceTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment)
	:Vrui::TransformTool(factory,inputAssignment),moveUp(false),moveDown(false)
	{
	}

void medicalViewer::ChangeSliceTool::buttonCallback(int btnIndex,Vrui::InputDevice::ButtonCallbackData* cbData)
	{
		switch(btnIndex)
		{
			case 0:
				moveUp = cbData->newButtonState;
				break;
			case 1:
				moveDown = cbData->newButtonState;
				break;
		}
	}
/*void medicalViewer::ChangeSliceTool::valuatorCallback(int valuatorSlotIndex,Vrui::InputDevice::ValuatorCallbackData* cbData)
	{
		std::cout << "VALUATOR: ";
		std::cout << "Old value: " << cbData->oldValuatorValue << "Old value: " << cbData->newValuatorValue << std::endl;
	}*/
void medicalViewer::ChangeSliceTool::frame()
{
	if(application->dicomLoaded && !(moveUp && moveDown))
	{
		if(moveUp)
		{
			// Change the slice number and produce the corresponding RGBA slice
			if(application->sliceNumber<application->dicom->getSlicesNumber()-2) application->sliceNumber++;

			// Get the left upper corner (luc) of the current plane
			application->luc = application->dicom->getImagePlaneUpperLeftCornerCoordinates(application->sliceNumber);
			// Get the right lower corner (rlc) of the current plane
			application->rlc = application->dicom->getImagePlaneLowerRightCornerCoordinates(application->sliceNumber);

			application->image = application->dicom->getImageSlice(application->sliceNumber,application->dicomAlpha);
			dicomReader::RGBAImageType::RegionType desiredRegion;
			desiredRegion = application->image->GetLargestPossibleRegion();

			application->columnCount = desiredRegion.GetSize()[0];
			application->rowCount    = desiredRegion.GetSize()[1];

			if(application->dicomDialog) application->updateDicomDialog();

			/* Request another frame: */
			Vrui::scheduleUpdate(Vrui::getApplicationTime()+1.0/125.0);
		}

		if(moveDown)
		{
			// Change the slice number and produce the corresponding RGBA slice
			if(application->sliceNumber>0) application->sliceNumber--;

			// Get the left upper corner (luc) of the current plane
			application->luc = application->dicom->getImagePlaneUpperLeftCornerCoordinates(application->sliceNumber);
			// Get the right lower corner (rlc) of the current plane
			application->rlc = application->dicom->getImagePlaneLowerRightCornerCoordinates(application->sliceNumber);

			application->image = application->dicom->getImageSlice(application->sliceNumber,application->dicomAlpha);
			dicomReader::RGBAImageType::RegionType desiredRegion;
			desiredRegion = application->image->GetLargestPossibleRegion();

			application->columnCount = desiredRegion.GetSize()[0];
			application->rowCount    = desiredRegion.GetSize()[1];

			if(application->dicomDialog) application->updateDicomDialog();

			/* Request another frame: */
			Vrui::scheduleUpdate(Vrui::getApplicationTime()+1.0/125.0);
		}
	}
}


/************************************************************
Static elements of class medicalViewer::ModelProjectorTool:
************************************************************/

medicalViewer::ModelProjectorToolFactory* medicalViewer::ModelProjectorTool::factory=0;

/****************************************************
Methods of class medicalViewer::ModelProjectorTool:
****************************************************/

medicalViewer::ModelProjectorTool::ModelProjectorTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment)
	:Vrui::TransformTool(factory,inputAssignment)
	{
	/* Set the source input device: */
	sourceDevice=getButtonDevice(0);
	}

void medicalViewer::ModelProjectorTool::initialize(void)
	{
	/* Initialize the base tool: */
	TransformTool::initialize();
	
	/* Disable the transformed device's glyph: */
	Vrui::getInputGraphManager()->getInputDeviceGlyph(transformedDevice).disable();
	}

void medicalViewer::ModelProjectorTool::frame(void)
	{
	/* Calculate ray equation in navigation coordinates: */
	Vrui::Point p0=sourceDevice->getPosition();
	Vrui::Vector dir=sourceDevice->getRayDirection();
	p0-=dir*sourceDevice->getDeviceRayStart();
	Vrui::Point p1=p0+dir*Vrui::getBackplaneDist();
	PolygonModel::Point mp0=Vrui::getInverseNavigationTransformation().transform(p0);
	PolygonModel::Point mp1=Vrui::getInverseNavigationTransformation().transform(p1);
	
	/* Probe the model!: */
	transformedDevice->setDeviceRay(sourceDevice->getDeviceRayDirection(),sourceDevice->getDeviceRayStart());
	PolygonModel::Point mp=application->model->intersect(mp0,mp1);
	if(mp!=mp1)
		{
		/* Set the device position to the intersection point: */
		Vrui::TrackerState ts(Vrui::getNavigationTransformation().transform(mp)-Vrui::Point::origin,sourceDevice->getOrientation());
		transformedDevice->setTransformation(ts);
		}
	else
		{
		/* Move the device in the plane it currently inhabits: */
		Vrui::Scalar lambda=(dir*(transformedDevice->getPosition()-p0))/Geometry::sqr(dir);
		Vrui::TrackerState ts(p0+dir*lambda-Vrui::Point::origin,sourceDevice->getOrientation());
		transformedDevice->setTransformation(ts);
		}
	}

/************************************************
Methods of class medicalViewer::AlignmentState:
************************************************/

void medicalViewer::AlignmentState::set(PolygonModel::Scalar newPlayerHeight,const PolygonModel::Point& newPlayerFoot,PolygonModel::Scalar playerRadius)
	{
	/* Copy the parameters: */
	playerHeight=newPlayerHeight;
	playerFoot=newPlayerFoot;
	
	/* Initialize the player box in z-is-up model coordinates: */
	playerBox.min=playerBox.max=playerFoot;
	for(int i=0;i<2;++i)
		{
		playerBox.min[i]-=playerRadius;
		playerBox.max[i]+=playerRadius;
		}
	playerBox.max[2]+=playerHeight;
	}

/******************************************************
Methods of class VRVolumeRenderer::CuttingPlaneLocator:
******************************************************/

medicalViewer::CuttingPlaneLocator::CuttingPlaneLocator(Vrui::LocatorTool* sTool,GLuint sClipPlaneIndex)
	:Vrui::LocatorToolAdapter(sTool),
	 clipPlaneIndex(sClipPlaneIndex),
	 active(false)
	{
	}

void medicalViewer::CuttingPlaneLocator::motionCallback(Vrui::LocatorTool::MotionCallbackData* cbData)
	{
	/* Update the cutting plane's equation if it is active: */
	if(active)
		{
		planeNormal=cbData->currentTransformation.transform(Vrui::Vector(0,1,0));
		planeOffset=cbData->currentTransformation.getOrigin()*planeNormal;
		}
	}

void medicalViewer::CuttingPlaneLocator::buttonPressCallback(Vrui::LocatorTool::ButtonPressCallbackData* cbData)
	{
	/* Activate the cutting plane: */
	active=true;
	}

void medicalViewer::CuttingPlaneLocator::buttonReleaseCallback(Vrui::LocatorTool::ButtonReleaseCallbackData* cbData)
	{
	/* Deactivate the cutting plane: */
	active=false;
	}

void medicalViewer::CuttingPlaneLocator::setGLState(void) const
	{
	if(active)
		{
		/* Enable the clipping plane: */
		glEnable(GL_CLIP_PLANE0+clipPlaneIndex);

		/* Set the clipping plane's equation: */
		double comp[4];
		for(int i=0;i<3;++i)
			comp[i]=double(planeNormal[i]);
		comp[3]=double(-planeOffset);
		glClipPlane(GL_CLIP_PLANE0+clipPlaneIndex,comp);
		}
	}

void medicalViewer::CuttingPlaneLocator::resetGLState(void) const
	{
	if(active)
		{
		/* Disable the clipping plane: */
		glDisable(GL_CLIP_PLANE0+clipPlaneIndex);
		}
	}

namespace {

/****************
Helper functions:
****************/

inline float nudgePlus(float value)
	{
	union
		{
		float f;
		int i;
		} fi;
	fi.f=value;
	if(value>=0.0f)
		++fi.i;
	else
		--fi.i;
	return fi.f;
	}

inline double nudgePlus(double value)
	{
	union
		{
		double f;
		long int i;
		} fi;
	fi.f=value;
	if(value>=0.0)
		++fi.i;
	else
		--fi.i;
	return fi.f;
	}

inline float nudgeMinus(float value)
	{
	union
		{
		float f;
		int i;
		} fi;
	fi.f=value;
	if(value>0.0f)
		--fi.i;
	else
		++fi.i;
	return fi.f;
	}

inline double nudgeMinus(double value)
	{
	union
		{
		double f;
		long int i;
		} fi;
	fi.f=value;
	if(value>0.0)
		--fi.i;
	else
		++fi.i;
	return fi.f;
	}

template <class ScalarParam,int dimensionParam>
inline
Geometry::Point<ScalarParam,dimensionParam>
nudgePoint(
	const Geometry::Point<ScalarParam,dimensionParam>& point,
	const Geometry::Vector<ScalarParam,dimensionParam>& direction)
	{
	Geometry::Point<ScalarParam,dimensionParam> result;
	for(int i=0;i<dimensionParam;++i)
		{
		if(direction[i]>ScalarParam(0))
			result[i]=nudgePlus(point[i]);
		if(direction[i]<ScalarParam(0))
			result[i]=nudgeMinus(point[i]);
		}
	
	return result;
	}

}

/********************************
Methods of class medicalViewer:
********************************/

GLMotif::PopupMenu* medicalViewer::createMainMenu(void)
	{
	/* Create a popup shell to hold the main menu: */
	GLMotif::PopupMenu* mainMenuPopup=new GLMotif::PopupMenu("MainMenuPopup",Vrui::getWidgetManager());
	mainMenuPopup->setTitle("Medical Viewer");
	
	/* Create the main menu itself: */
	GLMotif::Menu* mainMenu=new GLMotif::Menu("MainMenu",mainMenuPopup,false);
	
	if(modelLoaded)
	{
		GLMotif::ToggleButton* showBackfacesButton=new GLMotif::ToggleButton("ShowBackfacesButton",mainMenu,"Show Backfaces");
		showBackfacesButton->setToggle(showBackfaces);
		showBackfacesButton->getValueChangedCallbacks().add(this,&medicalViewer::showBackfacesCallback);
	}
	
	GLMotif::Button* resetNavigationButton=new GLMotif::Button("ResetNavigationButton",mainMenu,"Reset Navigation");
	resetNavigationButton->getSelectCallbacks().add(this,&medicalViewer::resetNavigationCallback);
	
	GLMotif::Button* levelModelButton=new GLMotif::Button("LevelModelButton",mainMenu,"Level Model");
	levelModelButton->getSelectCallbacks().add(this,&medicalViewer::levelModelCallback);
	
	GLMotif::Button* scaleOneToOneButton=new GLMotif::Button("ScaleOneToOneButton",mainMenu,"Scale 1:1");
	scaleOneToOneButton->getSelectCallbacks().add(this,&medicalViewer::scaleOneToOneCallback);
	
	if(modelLoaded)
	{
		GLMotif::Button* meshesVisibilityButton=new GLMotif::Button("MeshesVisibilityButton",mainMenu,"Meshes visibility");
		meshesVisibilityButton->getSelectCallbacks().add(this,&medicalViewer::meshesVisibilityDialogCallback);
	}

	if(dicomLoaded)
	{
		GLMotif::Button* dicomButton=new GLMotif::Button("DicomButton",mainMenu,"DICOM Menu");
		dicomButton->getSelectCallbacks().add(this,&medicalViewer::dicomDialogCallback);
	}

	if(modelLoaded)
	{
		/* Create a cascade button to show the "Meshes" submenu: */
		GLMotif::CascadeButton* meshesCascade=new GLMotif::CascadeButton("SubmeshesCascade",mainMenu,"Meshes properties");
		meshesCascade->setPopup(createMeshesMenu());
	}

	if(volumeLoaded)
	{
		/* Create a cascade button to show the "Volume" submenu: */
		GLMotif::CascadeButton* volumeCascade=new GLMotif::CascadeButton("VolumeCascade",mainMenu,"Volume");
		volumeCascade->setPopup(createVolumeRenderingMenu());
	}

	/* Finish building the main menu: */
	mainMenu->manageChild();
	
	return mainMenuPopup;
	}

GLMotif::Popup* medicalViewer::createMeshesMenu(void)
{
	/* Create the submenu's top-level shell: */
	GLMotif::Popup* meshesMenuPopup=new GLMotif::Popup("MeshesMenuPopup",Vrui::getWidgetManager());

	/* Create the array of render toggle buttons inside the top-level shell: */
	GLMotif::SubMenu* meshesMenu=new GLMotif::SubMenu("MeshesMenu",meshesMenuPopup,false);

	/* Check if the model is a hierarchical triangle set or contains one: */
	const HierarchicalTriangleSetBase* hts=dynamic_cast<HierarchicalTriangleSetBase*>(model);
	if(hts==0)
		{
		MultiModel* mm=dynamic_cast<MultiModel*>(model);
		if(mm!=0)
			hts=mm->getHierarchicalTriangleSet();
		}
	if(hts!=0)
		{
			// Find all the submeshes: *
			HierarchicalTriangleSetBase::SubMeshesVectorPointersType meshes;
			meshes = hts->getSubMeshesPointers();

			// Create the submeshes menu
			for(int i=1;i<meshes.size();i++)
			{
				/* Create a unique name for the toggle button widget: */
				char buttonName[256];
				snprintf(buttonName,sizeof(buttonName),"%04d",i);

				GLMotif::Button* meshButton=new GLMotif::Button(buttonName,meshesMenu,meshes[i]->getName().c_str());
				meshButton->getSelectCallbacks().add(this,&medicalViewer::selectMeshCallback);
			}
		}
	else
		subMesh=0;

	/* Finish building the main menu: */
	meshesMenu->manageChild();

	/* Return the created top-level shell: */
	return meshesMenuPopup;
}


GLMotif::PopupWindow* medicalViewer::createSubMeshDialog(void)
	{
	/* Create the submesh dialog window: */
	GLMotif::PopupWindow* subMeshDialogPopup=new GLMotif::PopupWindow("SubMeshDialogPopup",Vrui::getWidgetManager(),"Submesh Data");
	subMeshDialogPopup->setResizableFlags(true,false);
	subMeshDialogPopup->setCloseButton(true);
	subMeshDialogPopup->getCloseCallbacks().add(this,&medicalViewer::subMeshDialogCloseCallback);

	GLMotif::RowColumn* data=new GLMotif::RowColumn("Data",subMeshDialogPopup,false);
	data->setOrientation(GLMotif::RowColumn::VERTICAL);
	data->setPacking(GLMotif::RowColumn::PACK_TIGHT);
	data->setNumMinorWidgets(2);

	new GLMotif::Label("NameLabel",data,"Name");
	nameField=new GLMotif::TextField("NameField",data,40);
	nameField->setHAlignment(GLFont::Left);

	new GLMotif::Label("VisibilityLabel",data,"Visibility");
	meshVisibilityButton = new GLMotif::ToggleButton("visibilityToggle",data," ");
	meshVisibilityButton->getValueChangedCallbacks().add(this,&medicalViewer::meshVisibilityCallback);

	bool enableTransparencySlider = true;
	for(int i=0;i<loadedMeshFiles.size();i++)
	{
		if(loadedMeshFiles[i].isTransparent && i!=selectedMesh)
			enableTransparencySlider = false;

		if(dicomAlpha != 255 || volumeLoaded) enableTransparencySlider = false;
	}


	if(enableTransparencySlider)
	{
		new GLMotif::Label("VisibilityLabel",data,"Opacity");
		surfaceTransparencySlider=new GLMotif::Slider("SurfaceTransparencySlider",data,GLMotif::Slider::HORIZONTAL,2.0f);
		surfaceTransparencySlider->setValueRange(0.0,1.0,0.02);
		surfaceTransparencySlider->getValueChangedCallbacks().add(this,&medicalViewer::sliderCallback);
	}
	else
	{
		new GLMotif::Label("VisibilityLabel",data,"Opacity");
		surfaceTransparencySlider=new GLMotif::Slider("SurfaceTransparencySlider",data,GLMotif::Slider::HORIZONTAL,2.0f);
		surfaceTransparencySlider->setValueRange(1,1,1);
	}

	new GLMotif::Label("NumTrianglesLabel",data,"Num Tris");

	GLMotif::Margin* numTrianglesMargin=new GLMotif::Margin("NumTrianglesMargin",data,false);
	numTrianglesMargin->setAlignment(GLMotif::Alignment(GLMotif::Alignment::LEFT));

	numTrianglesField=new GLMotif::TextField("NumTrianglesField",numTrianglesMargin,10);

	numTrianglesMargin->manageChild();

	new GLMotif::Label("BboxLabel",data,"Box");

	GLMotif::RowColumn* bbox=new GLMotif::RowColumn("Bbox",data,false);
	bbox->setOrientation(GLMotif::RowColumn::VERTICAL);
	bbox->setPacking(GLMotif::RowColumn::PACK_GRID);
	bbox->setNumMinorWidgets(3);

	for(int i=0;i<6;++i)
		{
		bboxField[i]=new GLMotif::TextField("BboxField",bbox,10);
		bboxField[i]->setFloatFormat(GLMotif::TextField::SMART);
		bboxField[i]->setPrecision(9);
		}

	bbox->manageChild();

	new GLMotif::Label("BboxCenterLabel",data,"Center");

	GLMotif::RowColumn* bboxCenter=new GLMotif::RowColumn("BboxCenter",data,false);
	bboxCenter->setOrientation(GLMotif::RowColumn::HORIZONTAL);
	bboxCenter->setPacking(GLMotif::RowColumn::PACK_GRID);
	bboxCenter->setNumMinorWidgets(1);

	for(int i=0;i<3;++i)
		{
		bboxCenterField[i]=new GLMotif::TextField("BboxCenterField",bboxCenter,10);
		bboxCenterField[i]->setFloatFormat(GLMotif::TextField::SMART);
		bboxCenterField[i]->setPrecision(9);
		}

	bboxCenter->manageChild();

	data->manageChild();

	return subMeshDialogPopup;
	}

void medicalViewer::sliderCallback(GLMotif::Slider::ValueChangedCallbackData* cbData)
	{
		if(strcmp(cbData->slider->getName(),"SurfaceTransparencySlider")==0)
		{
			// DEBUG TEXT
			//std::cout << "Slider moved. Value: " << cbData->value << std::endl;
			//surfaceTransparent=cbData->value<1.0;
			//surfaceMaterial.diffuse[3]=cbData->value;
			if(this->subMesh)
			{
				// Set the visibility of the mesh
				mutSubMesh->setSurfaceTransparent(cbData->value<1.0);
				if(cbData->value<1.0)
					loadedMeshFiles[selectedMesh].isTransparent = true;
				else
					loadedMeshFiles[selectedMesh].isTransparent = false;

				typedef Misc::Autopointer<PhongMaterial> MaterialPointer;
				const MaterialPointer mat = static_cast<MaterialPointer>(mutSubMesh->getMaterial());
				GLMaterial oldMat = mat->getFrontMaterial();
				// DEBUG TEXT
				//std::cout << "Material before: " << oldMat.diffuse[0] << "-" << oldMat.diffuse[1] << "-" << oldMat.diffuse[2] << "-" << oldMat.diffuse[3] << "\n";
				oldMat.diffuse[3] = cbData->value;
				//oldMat.ambient[3] = cbData->value;
				//oldMat.specular[3] = cbData->value;
				mat->setFrontMaterial(oldMat);
				oldMat.diffuse[3] = 0;
				oldMat.specular[3] = 0;
				oldMat.ambient[3] = 0;
				mat->setBackMaterial(oldMat);
				// DEBUG TEXT
				//GLMaterial newMat = mat->getFrontMaterial();
				//std::cout << "Material after: " << newMat.diffuse[0] << "-" << newMat.diffuse[1] << "-" << newMat.diffuse[2] << "-" << newMat.diffuse[3] << "\n";
			}
		}
		else if (strcmp(cbData->slider->getName(),"DicomOpacitySlider")==0)
		{
			// Change the alpha value of the image
			dicomAlpha = (unsigned char) 255 * cbData->value;
			image = dicom->getImageSlice(sliceNumber,dicomAlpha);
		}
		else if (strcmp(cbData->slider->getName(),"DicomSliceSelectionSlider")==0)
		{
			// Change the slice number and produce the corresponding RGBA slice
			sliceNumber = cbData->value;

			// Get the left upper corner (luc) of the current plane
			luc = dicom->getImagePlaneUpperLeftCornerCoordinates(sliceNumber);
			// Get the right lower corner (rlc) of the current plane
			rlc = dicom->getImagePlaneLowerRightCornerCoordinates(sliceNumber);

			image = dicom->getImageSlice(sliceNumber,dicomAlpha);
			dicomReader::RGBAImageType::RegionType desiredRegion;
			desiredRegion = image->GetLargestPossibleRegion();

			columnCount = desiredRegion.GetSize()[0];
			rowCount    = desiredRegion.GetSize()[1];

			updateDicomDialog();
		}
		else if (strcmp(cbData->slider->getName(),"DicomLowerLevelSlider")==0)
		{
			if(cbData->value < upperThreshold)
			{
				lowerThreshold = cbData->value;
			}
			else
			{
				lowerThreshold = cbData->value;
				upperThreshold = cbData->value;
				updateDicomDialog();
			}
			// Rebuild the image
			dicom->setLowerThreshold(lowerThreshold);
			dicom->setUpperThreshold(upperThreshold);
			image = dicom->getImageSlice(sliceNumber,dicomAlpha);
		}
		else if (strcmp(cbData->slider->getName(),"DicomUpperLevelSlider")==0)
		{
			if(cbData->value > lowerThreshold)
			{
				upperThreshold = cbData->value;
			}
			else
			{
				upperThreshold = cbData->value;
				lowerThreshold = cbData->value;
				updateDicomDialog();
			}
			// Rebuild the image
			dicom->setLowerThreshold(lowerThreshold);
			dicom->setUpperThreshold(upperThreshold);
			image = dicom->getImageSlice(sliceNumber,dicomAlpha);
		}
		else if (strcmp(cbData->slider->getName(),"SliceFactorSlider")==0)
		{
			/* Change the slice factor (and transparency gamma to keep total opacity constant): */
			double newSliceFactor=cbData->value;
			transparencyGamma=float((double(transparencyGamma)*newSliceFactor)/double(sliceFactor));
			sliceFactor=VolumeRenderer::Scalar(cbData->value);

			/* Update the slice factor value label: */
			char value[40];
			snprintf(value,sizeof(value),"%4.2lf",cbData->value);
			//sliceFactorValue->setLabel(value);

			/* Update the transparency gamma value label: */
			snprintf(value,sizeof(value),"%4.2lf",cbData->value);
			//transparencyGammaValue->setLabel(value);

			/* Update the transparency gamma slider: */
			transparencyGammaSlider->setValue(double(transparencyGamma));

			/* Update the volume renderer and color map: */
			renderer->setSliceFactor(sliceFactor);
			colorMapChangedCallback(0);
		}
		else if (strcmp(cbData->slider->getName(),"TransparencyGammaSlider")==0)
		{
			/* Change the transparency gamma factor: */
			transparencyGamma=GLfloat(cbData->value);

			/* Update the transparency gamma value label: */
			char value[40];
			snprintf(value,sizeof(value),"%4.2lf",cbData->value);
			//transparencyGammaValue->setLabel(value);

			/* Update the color map: */
			colorMapChangedCallback(0);
		}
	}

GLMotif::PopupWindow* medicalViewer::createMeshesVisibilityDialog(void)
	{
	/* Create the submesh dialog window: */
	GLMotif::PopupWindow* meshesVisibilityDialogPopup=new GLMotif::PopupWindow("MeshesVisibilityDialogPopup",Vrui::getWidgetManager(),"Select Meshes Visibility");
	meshesVisibilityDialogPopup->setResizableFlags(true,false);
	meshesVisibilityDialogPopup->setCloseButton(true);
	meshesVisibilityDialogPopup->getCloseCallbacks().add(this,&medicalViewer::meshesVisibilityDialogCloseCallback);

	GLMotif::RowColumn* data=new GLMotif::RowColumn("Data",meshesVisibilityDialogPopup,false);
	data->setOrientation(GLMotif::RowColumn::VERTICAL);
	data->setPacking(GLMotif::RowColumn::PACK_TIGHT);
	data->setNumMinorWidgets(2);

	/* Check if the model is a hierarchical triangle set or contains one: */
	const HierarchicalTriangleSetBase* hts=dynamic_cast<HierarchicalTriangleSetBase*>(model);
	if(hts==0)
		{
		MultiModel* mm=dynamic_cast<MultiModel*>(model);
		if(mm!=0)
			hts=mm->getHierarchicalTriangleSet();
		}
	if(hts!=0)
		{
			// Find all the submeshes: *
			HierarchicalTriangleSetBase::SubMeshesVectorPointersType meshes;
			meshes = hts->getSubMeshesPointers();

			if(!meshesVisibilityButtons.empty()) meshesVisibilityButtons.clear();

			// Create the submeshes menu
			for(int i=1;i<meshes.size();i++)
			{
				/* Create a unique name for the toggle button widget: */
				char buttonName[256];
				snprintf(buttonName,sizeof(buttonName),"%04d",i);

				GLMotif::ToggleButton* toggleButton = new GLMotif::ToggleButton(buttonName,data," ");
				toggleButton->setToggle(meshes[i]->getVisibility());
				toggleButton->getValueChangedCallbacks().add(this,&medicalViewer::meshesVisibilityCallback);
				meshesVisibilityButtons.push_back(toggleButton);
				new GLMotif::Label("VisibilityLabel",data,meshes[i]->getName().c_str());
			}
		}
	else
		subMesh=0;


	data->manageChild();

	return meshesVisibilityDialogPopup;
	}

GLMotif::PopupWindow* medicalViewer::createDicomDialog(void)
	{
	/* Create the submesh dialog window: */
	GLMotif::PopupWindow* dicomDialogPopup=new GLMotif::PopupWindow("DicomDialogPopup",Vrui::getWidgetManager(),"DICOM Menu");
	dicomDialogPopup->setResizableFlags(true,false);
	dicomDialogPopup->setCloseButton(true);
	dicomDialogPopup->getCloseCallbacks().add(this,&medicalViewer::dicomDialogCloseCallback);

	GLMotif::RowColumn* data=new GLMotif::RowColumn("Data",dicomDialogPopup,false);
	data->setOrientation(GLMotif::RowColumn::VERTICAL);
	data->setPacking(GLMotif::RowColumn::PACK_TIGHT);
	data->setNumMinorWidgets(2);

	// Try to get the modality name
	std::string modality = dicom->getModality();
	if(modality.size())
	{
		new GLMotif::Label("ModalityLabel",data,"Modality");
		new GLMotif::Label("ModalityLabel1",data,modality.c_str());
	}

	// Try to get the modality manifacturer/model
	std::string manifacturer = dicom->getManifacturerName();
	std::string equipment = dicom->getEquipmentName();
	std::string maniequipment = manifacturer + " / " + equipment;
	if(manifacturer.size() || equipment.size())
	{
		new GLMotif::Label("ManEquiLabel",data,"Manifacturer/Eq. name");
		new GLMotif::Label("ManEquiLabel1",data,maniequipment.c_str());
	}

	// Image spatial data
	char number[5];
	snprintf(number,sizeof(number),"%03f",dicom->getColumnSpacing());
	std::string columnSpacing = std::string(number);
	snprintf(number,sizeof(number),"%03f",dicom->getRowSpacing());
	std::string rowSpacing = std::string(number);
	snprintf(number,sizeof(number),"%03f",dicom->getSliceThickness());
	std::string sliceThickness = std::string(number);
	std::string spatialData = "[ " + columnSpacing + " , " + rowSpacing + " , " + sliceThickness + " ]";
	if(columnSpacing.size() && rowSpacing.size() && sliceThickness.size())
	{
		new GLMotif::Label("ManEquiLabel",data,"Voxel spacing");
		new GLMotif::Label("ManEquiLabel1",data,spatialData.c_str());
	}

	// Image resolution
	snprintf(number,sizeof(number),"%d",dicom->getColumnNumber());
	std::string columnNumber = std::string(number);
	snprintf(number,sizeof(number),"%d",dicom->getRowNumber());
	std::string rowNumber = std::string(number);
	std::string resolutionData = "[ " + columnNumber + " , " + rowNumber + " ]";
	if(columnNumber.size() && rowNumber.size())
	{
		new GLMotif::Label("ManEquiLabel",data,"Image resolution");
		new GLMotif::Label("ManEquiLabel1",data,resolutionData.c_str());
	}

	// Try to get the patient sex
	std::string sex = dicom->getPatientSex();
	if(sex.size())
	{
		new GLMotif::Label("SexLabel",data,"Patient Sex");
		new GLMotif::Label("SexLabel1",data,sex.c_str());
	}

	// Try to get the patient age
	/*std::string age = dicom->getPatientAge();
	if(age.size())
	{
		new GLMotif::Label("SexLabel",data,"Patient Sex");
		new GLMotif::Label("SexLabel1",data,age.c_str());
	}*/



	new GLMotif::Label("VisibilityLabel",data,"Visibility");
	dicomVisibilityButton = new GLMotif::ToggleButton("visibilityToggle",data," ");
	dicomVisibilityButton->setToggle(dicomVisible);
	dicomVisibilityButton->getValueChangedCallbacks().add(this,&medicalViewer::dicomVisibilityCallback);

	new GLMotif::Label("VisibilityLabel",data,"Opacity");
	dicomTransparencySlider=new GLMotif::Slider("DicomOpacitySlider",data,GLMotif::Slider::HORIZONTAL,2.0f);
	dicomTransparencySlider->setValueRange(0.0,1.0,0.02);
	dicomTransparencySlider->setValue((double)dicomAlpha/255.00);
	dicomTransparencySlider->getValueChangedCallbacks().add(this,&medicalViewer::sliderCallback);

	bool disableTransparencySlider = false;
	for(int i=0;i<loadedMeshFiles.size();i++)
	{
		if(loadedMeshFiles[i].isTransparent)
			disableTransparencySlider = true;
	}
	// If the volume is loaded disable the trasparency slider
	if(volumeLoaded || disableTransparencySlider) dicomTransparencySlider->setValueRange(1,1,1);

	char SliceIndexText[20];
	snprintf(SliceIndexText,sizeof(SliceIndexText),"Slice [ %d / %d ]",sliceNumber+1,dicom->getSlicesNumber());
	dicomSliceSliderLabel = new GLMotif::Label("VisibilityLabel",data,SliceIndexText);

	dicomSliceSlider=new GLMotif::Slider("DicomSliceSelectionSlider",data,GLMotif::Slider::HORIZONTAL,2.0f);
	dicomSliceSlider->setValueRange(0,dicom->getSlicesNumber()-1,1);
	dicomSliceSlider->setValue(sliceNumber);
	dicomSliceSlider->getValueChangedCallbacks().add(this,&medicalViewer::sliderCallback);

	new GLMotif::Label("WindowLabel",data,"Lower window level");
	dicomLowerLevelSlider=new GLMotif::Slider("DicomLowerLevelSlider",data,GLMotif::Slider::HORIZONTAL,2.0f);
	dicomLowerLevelSlider->setValueRange(MINIMUN_ALLOWED_THRESHOLD,MAXIMUM_ALLOWED_THRESHOLD,1);
	dicomLowerLevelSlider->setValue(lowerThreshold);
	dicomLowerLevelSlider->getValueChangedCallbacks().add(this,&medicalViewer::sliderCallback);

	new GLMotif::Label("WindowLabel",data,"Upper window level");
	dicomUpperLevelSlider=new GLMotif::Slider("DicomUpperLevelSlider",data,GLMotif::Slider::HORIZONTAL,2.0f);
	dicomUpperLevelSlider->setValueRange(MINIMUN_ALLOWED_THRESHOLD,MAXIMUM_ALLOWED_THRESHOLD,1);
	dicomUpperLevelSlider->setValue(upperThreshold);
	dicomUpperLevelSlider->getValueChangedCallbacks().add(this,&medicalViewer::sliderCallback);

	data->manageChild();

	return dicomDialogPopup;
	}

GLMotif::Popup* medicalViewer::createVolumeRenderingMenu(void)
	{
	const GLMotif::StyleSheet& ss=*Vrui::getWidgetManager()->getStyleSheet();

	/* Create the submenu's top-level shell: */
	GLMotif::Popup* volumeRenderingMenuPopup=new GLMotif::Popup("VolumeRenderingMenuPopup",Vrui::getWidgetManager());

	volumeRenderingMenuPopup->setBorderWidth(0.0f);
	volumeRenderingMenuPopup->setBorderType(GLMotif::Widget::RAISED);
	volumeRenderingMenuPopup->setBorderColor(ss.bgColor);
	volumeRenderingMenuPopup->setBackgroundColor(ss.bgColor);
	volumeRenderingMenuPopup->setForegroundColor(ss.fgColor);
	volumeRenderingMenuPopup->setMarginWidth(ss.size);
	volumeRenderingMenuPopup->setTitleSpacing(ss.size);
	volumeRenderingMenuPopup->setTitle("VR Volume Renderer",ss.font);

	/* Create the array of render toggle buttons inside the top-level shell: */
	GLMotif::SubMenu* volumeRenderingMenu=new GLMotif::SubMenu("volumeRenderingMenu",volumeRenderingMenuPopup,false);
	volumeRenderingMenu->setBorderWidth(0.0f);
	volumeRenderingMenu->setOrientation(GLMotif::RowColumn::VERTICAL);
	volumeRenderingMenu->setNumMinorWidgets(1);
	volumeRenderingMenu->setMarginWidth(0.0f);
	volumeRenderingMenu->setSpacing(ss.size);

	GLMotif::Button* centerDisplayButton=new GLMotif::Button("CenterDisplayButton",volumeRenderingMenu,"Center Display",ss.font);
	centerDisplayButton->getSelectCallbacks().add(this,&medicalViewer::centerDisplayCallback);

	GLMotif::ToggleButton* showPaletteEditorToggle=new GLMotif::ToggleButton("ShowPaletteEditorToggle",volumeRenderingMenu,"Show Palette Editor",ss.font);
	showPaletteEditorToggle->getValueChangedCallbacks().add(this,&medicalViewer::showPaletteEditorCallback);
	showPaletteEditorToggle->setToggle(false);

	GLMotif::Button* savePaletteButton=new GLMotif::Button("SavePaletteButton",volumeRenderingMenu,"Save Palette",ss.font);
	savePaletteButton->getSelectCallbacks().add(this,&medicalViewer::savePaletteCallback);

	GLMotif::ToggleButton* showRenderSettingsDialogToggle=new GLMotif::ToggleButton("ShowRenderSettingsDialogToggle",volumeRenderingMenu,"Show Render Settings Dialog",ss.font);
	showRenderSettingsDialogToggle->getValueChangedCallbacks().add(this,&medicalViewer::showRenderSettingsDialogCallback);
	showRenderSettingsDialogToggle->setToggle(false);

	GLMotif::Button* createInputDeviceButton=new GLMotif::Button("CreateInputDeviceButton",volumeRenderingMenu,"Create Input Device",ss.font);
	createInputDeviceButton->getSelectCallbacks().add(this,&medicalViewer::createInputDeviceCallback);

	GLMotif::Button* saveViewButton=new GLMotif::Button("SaveViewButton",volumeRenderingMenu,"Save View",ss.font);
	saveViewButton->getSelectCallbacks().add(this,&medicalViewer::saveViewCallback);

	GLMotif::Button* loadViewButton=new GLMotif::Button("LoadViewButton",volumeRenderingMenu,"Load View",ss.font);
	loadViewButton->getSelectCallbacks().add(this,&medicalViewer::loadViewCallback);

	volumeRenderingMenu->manageChild();

	return volumeRenderingMenuPopup;
	}

GLMotif::PopupWindow* medicalViewer::createRenderSettingsDialog(void)
	{
	const GLMotif::StyleSheet& ss=*Vrui::getWidgetManager()->getStyleSheet();

	GLMotif::PopupWindow* renderSettingsDialog=new GLMotif::PopupWindow("RenderSettingsDialog",Vrui::getWidgetManager(),"Rendering Settings",ss.font);
	renderSettingsDialog->setBorderColor(ss.bgColor);
	renderSettingsDialog->setBackgroundColor(ss.bgColor);
	renderSettingsDialog->setForegroundColor(ss.fgColor);
	renderSettingsDialog->setTitleBarColor(ss.titlebarBgColor);
	renderSettingsDialog->setTitleBarTextColor(ss.titlebarFgColor);
	renderSettingsDialog->setChildBorderWidth(ss.size);

	GLMotif::RowColumn* renderSettings=new GLMotif::RowColumn("RenderSettings",renderSettingsDialog,false);
	renderSettings->setBorderWidth(0.0f);
	renderSettings->setOrientation(GLMotif::RowColumn::VERTICAL);
	renderSettings->setNumMinorWidgets(3);
	renderSettings->setMarginWidth(0.0f);
	renderSettings->setSpacing(ss.size);

	/* Create a slider/textfield combo to change the slice factor: */
	GLMotif::Label* sliceFactorLabel=new GLMotif::Label("SliceFactorLabel",renderSettings,"Slice Factor",ss.font);

	char sliceFactorValueString[40];
	snprintf(sliceFactorValueString,sizeof(sliceFactorValueString),"%4.2lf",double(sliceFactor));
	sliceFactorValue=new GLMotif::Label("SliceFactorValue",renderSettings,sliceFactorValueString,ss.font);
	sliceFactorValue->setBorderWidth(ss.size*0.5f);
	sliceFactorValue->setBorderType(GLMotif::Widget::LOWERED);
	sliceFactorValue->setBackgroundColor(ss.textfieldBgColor);
	sliceFactorValue->setForegroundColor(ss.textfieldFgColor);
	sliceFactorValue->setMarginWidth(ss.size*0.5f);
	sliceFactorValue->setHAlignment(GLFont::Right);

	sliceFactorSlider=new GLMotif::Slider("SliceFactorSlider",renderSettings,GLMotif::Slider::HORIZONTAL,ss.sliderHandleWidth,ss.fontHeight*10.0f);
	sliceFactorSlider->setSliderColor(ss.sliderHandleColor);
	sliceFactorSlider->setShaftColor(ss.sliderShaftColor);
	sliceFactorSlider->setValueRange(0.1,4.0,0.01);
	sliceFactorSlider->setValue(double(sliceFactor));
	sliceFactorSlider->getValueChangedCallbacks().add(this,&medicalViewer::sliderCallback);

	/* Create a slider/textfield combo to change the transparency gamma factor: */
	GLMotif::Label* transparencyGammaLabel=new GLMotif::Label("TransparencyGammaLabel",renderSettings,"Transparency Gamma",ss.font);

	char transparencyGammaValueString[40];
	snprintf(transparencyGammaValueString,sizeof(transparencyGammaValueString),"%4.2lf",double(transparencyGamma));
	transparencyGammaValue=new GLMotif::Label("TransparencyGammaValue",renderSettings,transparencyGammaValueString,ss.font);
	transparencyGammaValue->setBorderWidth(ss.size*0.5f);
	transparencyGammaValue->setBorderType(GLMotif::Widget::LOWERED);
	transparencyGammaValue->setBackgroundColor(ss.textfieldBgColor);
	transparencyGammaValue->setForegroundColor(ss.textfieldFgColor);
	transparencyGammaValue->setMarginWidth(ss.size*0.5f);
	transparencyGammaValue->setHAlignment(GLFont::Right);

	transparencyGammaSlider=new GLMotif::Slider("TransparencyGammaSlider",renderSettings,GLMotif::Slider::HORIZONTAL,ss.sliderHandleWidth,ss.fontHeight*10.0f);
	transparencyGammaSlider->setSliderColor(ss.sliderHandleColor);
	transparencyGammaSlider->setShaftColor(ss.sliderShaftColor);
	transparencyGammaSlider->setValueRange(0.1,4.0,0.01);
	transparencyGammaSlider->setValue(double(transparencyGamma));
	transparencyGammaSlider->getValueChangedCallbacks().add(this,&medicalViewer::sliderCallback);

	renderSettings->manageChild();

	return renderSettingsDialog;
	}


medicalViewer::medicalViewer(int& argc,char**& argv)
	:Vrui::Application(argc,argv),
	 materialManager(0),
	 model(0),
	 upVector(0,0,1),
	 showBackfaces(false),
	 subMesh(0),
	 mainMenu(0),subMeshDialog(0),meshesVisibilityDialog(0),dicomDialog(0),selectedMesh(-1),
	 dicom(0),rowCount(0),columnCount(0),sliceNumber(0),
	 lowerThreshold(0),upperThreshold(255),
	 dicomLoaded(false), dicomVisible(false), dicomAlpha(255), modelLoaded(false),
	 volumeLoaded(false), volumeVisible(false), pUsage(false),
	 renderer(0),palette(0),sliceFactor(1),transparencyGamma(1.0f),
	 volumeRenderingMenu(0),
	 paletteEditor(0),
	 renderSettingsDialog(0),sliceFactorValue(0),transparencyGammaValue(0),
	 numClipPlanes(0),clipPlaneAllocateds(0),rotationAngle(0),rotateModels(false)
	{

	enum FileMode
			{
			MODELNAMEFILE,DIRECTORYNAME,DICOMDIRECTORY,VOLUMENAME,
			PALETTENAME,VIEWMODE,SLICEMODE
			} fileMode=MODELNAMEFILE;

	/* Parse the command line: */
	const char* imagePrefix="";
	const char* imageReplace="";
	std::vector<const char*> modelFileNames;
	std::vector<const char*> modelDirectories;
	const char* bspTreeFileName=0;
	char* volumeFileName=0;
	char* paletteFileName=0;
	char* viewFileName=0;
	Geometry::LinearUnit linearUnit;
	for(int i=1;i<argc;++i)
	{
		if(argv[i][0]=='-')
			{
			if(strcasecmp(argv[i]+1,"prefix")==0)
				{
				++i;
				imagePrefix=argv[i];
				}
			else if(strcasecmp(argv[i]+1,"replace")==0)
				{
				++i;
				imageReplace=argv[i];
				}
			else if(strcasecmp(argv[i]+1,"bsp")==0)
				{
				++i;
				bspTreeFileName=argv[i];
				}
			else if(strcasecmp(argv[i]+1,"up")==0)
				{
				for(int j=0;j<3;++j)
					{
					++i;
					upVector[j]=Vrui::Scalar(atof(argv[i]));
					}
				upVector.normalize();
				}
			else if(strcasecmp(argv[i]+1,"unit")==0)
				{
				/* Override the default linear unit from the command line: */
				++i;
				double unitFactor=atof(argv[i]);
				++i;
				linearUnit=Geometry::LinearUnit(argv[i],unitFactor);
				}
			else if(strcasecmp(argv[i]+1,"d")==0)
				{
					/* Load files from directories switch */
					fileMode = DIRECTORYNAME;
				}
			else if(strcasecmp(argv[i]+1,"dd")==0)
				{
					/* Load files from directories switch */
					fileMode = DICOMDIRECTORY;
				}
			else if(strcasecmp(argv[i]+1,"cv")==0)
				{
					// If next argument isn't a switch increment the counter to get it as filename
					if(i<argc-1 && argv[i+1][0]!='-') i++;

					if(dicomLoaded)
					{
						// Check if to use the default name (no name provided)
						if(argv[i][0]=='-')
						{
							// Create a volume file with the same name of the dicom directory
							dicom->createVolFile();
						}
						else
						{
							// Create a volume file with the given name
							dicom->createVolFile(argv[i]);
						}
					}
					else
					{
						std::cout << "Cannot create a volume without DICOM files loaded!" << std::endl;
					}
				}
			else if(strcasecmp(argv[i]+1,"v")==0)
				{
					/* Volume name given */
					fileMode = VOLUMENAME;
				}
			else if(strcasecmp(argv[i]+1,"p")==0)
				{
					/* Load files from directories switch */
					fileMode = PALETTENAME;
				}
			else if(strcasecmp(argv[i]+1,"h")==0)
			{
				pUsage = true;
			}
			else if(strcasecmp(argv[i]+1,"-HELP")==0)
			{
				pUsage = true;
			}
			else if(strcasecmp(argv[i]+1,"VIEW")==0)
				{
					/* Volume name given */
					fileMode = VIEWMODE;
				}
			else if(strcasecmp(argv[i]+1,"SLICE")==0)
				{
					/* Volume name given */
					fileMode = SLICEMODE;
				}
			else
				std::cout<<"Unrecognized switch "<<argv[i]<<std::endl;
			}
		else if(fileMode == DIRECTORYNAME)
		{
			while(i<argc)
			{
				modelDirectories.push_back(argv[i]);
				++i;

				// If the next argument exists and is a switch
				// decrease i (will be then increased in the main for)
				// and exit from the while(i<argc)
				if(i<argc && argv[i][0]=='-')
				{
					--i;
					break;
				}
			}

			// Switch to the standard behaviour
			fileMode = MODELNAMEFILE;
		}
		else if(fileMode == DICOMDIRECTORY)
		{
			// Call the DICOM reader giving the directory name
			if(!dicom) dicom = new dicomReader(argv[i]);

			// Try to read the DICOM files
			std::cout << "Loading DICOM files..." << std::endl;
			dicomLoaded = dicom->readDicomDirectory();
			if(dicomLoaded)
			{
				std::cout << "DICOM files loaded successfully" << std::endl;

				// Set the middle slice as default
				sliceNumber = dicom->getSlicesNumber()/2;

				// Set the default values of the threshold
				lowerThreshold = DEFAULT_LOWER_THRESHOLD;
				upperThreshold = DEFAULT_UPPER_THRESHOLD;

				// Set the conversion threshold values
				dicom->setLowerThreshold(lowerThreshold);
				dicom->setUpperThreshold(upperThreshold);

				// Get the left upper corner (luc) of the current plane
				luc = dicom->getImagePlaneUpperLeftCornerCoordinates(sliceNumber);
				// Get the right lower corner (rlc) of the current plane
				rlc = dicom->getImagePlaneLowerRightCornerCoordinates(sliceNumber);

				image = dicom->getImageSlice(sliceNumber,dicomAlpha);
				dicomReader::RGBAImageType::RegionType desiredRegion;
				desiredRegion = image->GetLargestPossibleRegion();

				columnCount = desiredRegion.GetSize()[0];
				rowCount    = desiredRegion.GetSize()[1];

				dicomVisible = true;

				// Change the application upvector
				//upVector = Vrui::Vector(0,0,-1);
			}
			else
				std::cout << "Error in loading DICOM directory" << std::endl;

			// Switch to the standard behaviour
			fileMode = MODELNAMEFILE;
		}
		else if(fileMode == VOLUMENAME)
		{
			// Get the volume filename
			if(!volumeFileName) volumeFileName=argv[i];

			// Switch to the standard behaviour
			fileMode = MODELNAMEFILE;
		}
		else if(fileMode == PALETTENAME)
		{
			// Get the palette filename
			if(!paletteFileName) paletteFileName=argv[i];

			// Switch to the standard behaviour
			fileMode = MODELNAMEFILE;
		}
		else if(fileMode == VIEWMODE)
		{
			// Get the volume filename
			if(!viewFileName) viewFileName=argv[i];

			// Switch to the standard behaviour
			fileMode = MODELNAMEFILE;
		}
		else if(fileMode == SLICEMODE)
		{
			// Set the sloce factor
			sliceFactor=atof(argv[i]);
			transparencyGamma=float(sliceFactor);
		}
		else if(fileMode == MODELNAMEFILE)
		{
			// Divide the filename from its extension
			unsigned dotIndex       = std::string(argv[i]).find_last_of(".");
			unsigned lastSlashIndex = std::string(argv[i]).find_last_of("/");
			std::string directory = "./";
			if(lastSlashIndex<std::string(argv[i]).length()) directory = std::string(argv[i]).substr(0,lastSlashIndex+1);
			std::string filename = std::string(argv[i]).substr(lastSlashIndex+1,dotIndex-lastSlashIndex-1);
			std::string ext = std::string(argv[i]).substr(dotIndex+1,std::string(argv[i]).length());

			// Check the extension
			if(ext=="ply" || ext=="lwo" || ext=="lws" || ext=="ase" || ext=="obj")
			{
				filePar file;
				file.isValid   = false;
				file.isTransparent = false;
				file.part      = 0;
				file.directory = directory;
				file.filename  = filename;
				file.extension = ext;
				file.path      = file.directory + file.filename + "." + file.extension;
				file.index     = loadedMeshFiles.size();
				loadedMeshFiles.push_back(file);
				modelFileNames.push_back(loadedMeshFiles.back().path.c_str());
#ifdef VERBOSE_OUTPUT
				std::cout << loadedMeshFiles.back().filename << "." << loadedMeshFiles.back().extension << "\n";
#endif
			}
		}
	}
	if(!modelDirectories.empty())
	{
		struct dirent *directoryContent;
		DIR *dir;
#ifdef VERBOSE_OUTPUT
		std::cout << "Parsing directories...\n";
#endif
		for(std::vector<const char*>::iterator mdIt=modelDirectories.begin();mdIt!=modelDirectories.end();++mdIt)
		{
			dir = opendir(*mdIt);
			if(!dir)
				std::cout << "Cannot open directory \"" << *mdIt << "\"\n";
			else
			{
#ifdef VERBOSE_OUTPUT
					std::cout << "Valid mesh entries for directory \"" << *mdIt << "\" are:\n";
#endif
				unsigned short int fileCounter;
				fileCounter = 0;

				// Convert the directory name
				std::string directory(*mdIt,strlen(*mdIt));
				directory += directory.at(directory.length()-1)!='/'?"/":"";

				while((directoryContent = readdir(dir)) != NULL)
				{
					// Divide the filename from its extension
					unsigned dotIndex = std::string(directoryContent->d_name).find_last_of(".");
					std::string filename = std::string(directoryContent->d_name).substr(0,dotIndex);
					std::string ext = std::string(directoryContent->d_name).substr(dotIndex+1,std::string(directoryContent->d_name).length());

					// Check the extension
					if(ext=="ply" || ext=="lwo" || ext=="lws" || ext=="ase" || ext=="obj")
					{
						++fileCounter;
						filePar file;
						file.isValid   = false;
						file.isTransparent = false;
						file.part      = 0;
						file.directory = directory;
						file.filename  = filename;
						file.extension = ext;
						file.path      = file.directory + file.filename + "." + file.extension;
						file.index     = loadedMeshFiles.size();
						loadedMeshFiles.push_back(file);
						modelFileNames.push_back(loadedMeshFiles.back().path.c_str());
#ifdef VERBOSE_OUTPUT
						std::cout << loadedMeshFiles.back().filename << "." << loadedMeshFiles.back().extension << "\n";
#endif
					}
				}
#ifdef VERBOSE_OUTPUT
				if(!fileCounter) std::cout << "None\n";
#endif
			}
		}
	}

	if(volumeFileName)
	{
		std::cout << "Loading volume file" << std::endl;

		/* Load palette renderer from file: */
		renderer=new PaletteRenderer(volumeFileName);

		/* Initialize renderer settings: */
		// renderer->setUseNPOTDTextures(true);
		renderer->setVoxelAlignment(VolumeRenderer::CELL_CENTERED);
		// renderer->setRenderingMode(VolumeRenderer::AXIS_ALIGNED);
		renderer->setRenderingMode(VolumeRenderer::VIEW_PERPENDICULAR);
		renderer->setInterpolationMode(VolumeRenderer::LINEAR);
		renderer->setTextureFunction(VolumeRenderer::REPLACE);
		renderer->setSliceFactor(sliceFactor);
		renderer->setAutosaveGLState(true);
		renderer->setTextureCaching(true);
		renderer->setSharePalette(false);

		/* Initialize the palette: */
		palette=new GLColorMap(GLColorMap::RAINBOW|GLColorMap::RAMP_ALPHA,1.0f,1.0f,0.0,255.0);
		paletteEditor=new PaletteEditor;
		paletteEditor->getColorMapChangedCallbacks().add(this,&medicalViewer::colorMapChangedCallback);
		if(paletteFileName!=0)
			paletteEditor->loadPalette(paletteFileName);
		else
			paletteEditor->createPalette(GLMotif::ColorMap::RAINBOW,0.0,255.0);

		/* Create the render settings dialog: */
		renderSettingsDialog=createRenderSettingsDialog();

		/* Initialize navigation transformation: */
		if(viewFileName!=0)
		{
			/* Open viewpoint file: */
			Misc::File viewpointFile(viewFileName,"rb",Misc::File::LittleEndian);

			/* Read the navigation transformation: */
			Vrui::NavTransform::Vector translation;
			viewpointFile.read(translation.getComponents(),3);
			Vrui::NavTransform::Rotation::Scalar quaternion[4];
			viewpointFile.read(quaternion,4);
			Vrui::NavTransform::Scalar scaling=viewpointFile.read<Vrui::NavTransform::Scalar>();

			/* Set the navigation transformation: */
			Vrui::setNavigationTransformation(Vrui::NavTransform(translation,Vrui::NavTransform::Rotation::fromQuaternion(quaternion),scaling));
		}
		else
			centerDisplayCallback(0);

		/* Create the OpenGL clipping plane allocation array: */
		// glGetIntegerv(GL_MAX_CLIP_PLANES,&numClipPlanes);
		numClipPlanes=6;
		clipPlaneAllocateds=new bool[numClipPlanes];
		for(int i=0;i<numClipPlanes;++i)
			clipPlaneAllocateds[i]=false;

		// Flag the volume loaded
		if(renderer)
		{
			volumeLoaded = true;
			std::cout << "Volume file loaded successfully" << std::endl;
		}
		else
			std::cout << "Can't load volume file" << std::endl;
	}

	// There are models
	if(!modelFileNames.empty()) modelLoaded = true;

	if( (!modelLoaded && !dicomLoaded && !volumeLoaded) || pUsage )
	{
		printUsage();
		Vrui::shutdown();
		//Misc::throwStdErr("Error: No input data given");
	}

	if(modelLoaded)
	{

		/* Register the custom tool classes with the Vrui tool manager: */
		ModelProbeToolFactory* toolFactory1=new ModelProbeToolFactory("ModelProbeTool","Model Probe",0,*Vrui::getToolManager());
		toolFactory1->setNumButtons(1);
		toolFactory1->setButtonFunction(0,"Probe Model");
		Vrui::getToolManager()->addClass(toolFactory1,Vrui::ToolManager::defaultToolFactoryDestructor);

		ModelProjectorToolFactory* toolFactory2=new ModelProjectorToolFactory("ModelProjectorTool","Model Projector",Vrui::getToolManager()->loadClass("TransformTool"),*Vrui::getToolManager());
		toolFactory2->setNumButtons(0,true);
		toolFactory2->setButtonFunction(0,"Forwarded Button");
		toolFactory2->setNumValuators(0,true);
		toolFactory2->setValuatorFunction(0,"Forwarded Valuator");
		Vrui::getToolManager()->addClass(toolFactory2,Vrui::ToolManager::defaultToolFactoryDestructor);

		RotationToolFactory* toolFactory3=new RotationToolFactory("RotationTool","Rotation tool",0,*Vrui::getToolManager());
		toolFactory3->setNumButtons(1);
		toolFactory3->setButtonFunction(0,"Rotate Model");
		Vrui::getToolManager()->addClass(toolFactory3,Vrui::ToolManager::defaultToolFactoryDestructor);

		ChangeSliceToolFactory* toolFactory4=new ChangeSliceToolFactory("ChangeSliceTool","Change slice tool",Vrui::getToolManager()->loadClass("TransformTool"),*Vrui::getToolManager());
		toolFactory4->setNumButtons(2);
		toolFactory4->setButtonFunction(0,"Decrease slice number");
		toolFactory4->setButtonFunction(1,"Increase slice number");
		//toolFactory2->setNumValuators(1);
		//toolFactory2->setValuatorFunction(0,"Change slice number");
		Vrui::getToolManager()->addClass(toolFactory4,Vrui::ToolManager::defaultToolFactoryDestructor);


		/* Create a material manager: */
		materialManager=new MaterialManager(imagePrefix,imageReplace);

		// Load model files
		MultiModel* mm=0;
		std::vector<const char*> objModelFileNames;
		for(int i=0;i<loadedMeshFiles.size();i++)
		{
			if(loadedMeshFiles[i].extension == "ply")
			{
				loadedMeshFiles[i].part=readPLYFile(loadedMeshFiles[i].path.c_str(),Vrui::getClusterMultiplexer());
			}
			else if(loadedMeshFiles[i].extension == "lwo")
			{
				loadedMeshFiles[i].part=readLWOFile(loadedMeshFiles[i].path.c_str(),*materialManager,Vrui::getClusterMultiplexer());
			}
			else if(loadedMeshFiles[i].extension == "lws")
			{
				loadedMeshFiles[i].part=readLWSFile(loadedMeshFiles[i].path.c_str(),*materialManager,Vrui::getClusterMultiplexer());
			}
			else if(loadedMeshFiles[i].extension == "ase")
			{
				loadedMeshFiles[i].part=readASEFile(loadedMeshFiles[i].path.c_str(),Vrui::getClusterMultiplexer());
			}
			else if(loadedMeshFiles[i].extension == "obj")
			{
				//loadedMeshFiles[i].part=readOBJFiles(loadedMeshFiles[i].path.c_str(),*materialManager,Vrui::getClusterMultiplexer());
				objModelFileNames.push_back(loadedMeshFiles[i].path.c_str());
			}
			else
				Misc::throwStdErr("MedicalViewer: Unrecognized extension in input file %s",loadedMeshFiles[i].path.c_str());

			// Add the part to the model
			if(loadedMeshFiles[i].part!=0)
			{
			if(mm!=0)
				mm->addPart(loadedMeshFiles[i].part);
			else if(model!=0)
				{
				mm=new MultiModel;
				mm->addPart(model);
				model=mm;
				}
			else
				model=loadedMeshFiles[i].part;
			}
		}

		PolygonModel* part = 0;
		if(objModelFileNames.size())
			part=readOBJFiles(objModelFileNames,*materialManager,Vrui::getClusterMultiplexer());

		if(part!=0)
			{
			if(mm!=0)
				mm->addPart(part);
			else if(model!=0)
				{
				mm=new MultiModel;
				mm->addPart(model);
				model=mm;
				}
			else
				model=part;
			}

		if(bspTreeFileName!=0)
			{
			/* Load a BSP tree: */
			std::cout<<"Loading BSP tree from "<<bspTreeFileName<<std::endl;
			model->loadBSPTree(bspTreeFileName);
			}
	
		/* Print the model's bounding box: */
		PolygonModel::Box bbox=model->calcBoundingBox();
		std::cout<<"Model bounding box: "<<bbox.min[0]<<" "<<bbox.min[1]<<" "<<bbox.min[2]<<" "<<bbox.max[0]<<" "<<bbox.max[1]<<" "<<bbox.max[2]<<std::endl;
	
		/* Calculate an epsilon value for collision detection: */
		PolygonModel::Scalar maxCoordinate=PolygonModel::Scalar(0);
		for(int i=0;i<3;++i)
			{
			if(maxCoordinate<Math::abs(bbox.min[i]))
				maxCoordinate=Math::abs(bbox.min[i]);
			if(maxCoordinate<Math::abs(bbox.max[i]))
				maxCoordinate=Math::abs(bbox.max[i]);
			}
		epsilon=nudgePlus(float(maxCoordinate))-float(maxCoordinate);
		std::cout<<"Collision detection epsilon is "<<epsilon<<std::endl;
	}
	
	/* Create the program's main menu: */
	mainMenu=createMainMenu();
	Vrui::setMainMenu(mainMenu);

	if(modelLoaded)
	{
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// Check if the model is a hierarchical triangle set or contains one: *
		const HierarchicalTriangleSetBase* hts=dynamic_cast<HierarchicalTriangleSetBase*>(model);
		if(hts==0)
			{
			MultiModel* mm=dynamic_cast<MultiModel*>(model);
			if(mm!=0)
				hts=mm->getHierarchicalTriangleSet();
			}
		if(hts!=0)
			{
				// Find all the submeshes: *
				HierarchicalTriangleSetBase::SubMeshesVectorPointersType meshes;
				meshes = hts->getSubMeshesPointers();
	#ifdef VERBOSE_OUTPUT
				std::cout<<"Submesh count: "<< meshes.size() <<std::endl;
				for(int i=0;i<meshes.size();i++)
				{
					std::cout << "Submesh ["<< i << "] name: " << meshes[i]->getName() << std::endl;
				}
	#endif
			}
		else
			subMesh=0;
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////
	}


	/* Set the linear unit: */
	Vrui::getCoordinateManager()->setUnit(linearUnit);
	
	/* Initialize the navigation transformation: */
	resetNavigationCallback(0);
	}

medicalViewer::~medicalViewer(void)
	{
	if(mainMenu) delete mainMenu;
	if(subMeshDialog) delete subMeshDialog;
	if(model) delete model;
	if(meshesVisibilityDialog) delete meshesVisibilityDialog;
	if(dicomDialog) delete dicomDialog;
	if(materialManager) delete materialManager;

	if(renderer) delete renderer;
	if(palette) delete palette;
	if(volumeRenderingMenu) delete volumeRenderingMenu;
	if(paletteEditor) delete paletteEditor;
	if(renderSettingsDialog) delete renderSettingsDialog;
	if(clipPlaneAllocateds) delete clipPlaneAllocateds;

	/* Delete all cutting plane tools: */
	for(std::vector<CuttingPlaneLocator*>::iterator cpIt=cuttingPlanes.begin();cpIt!=cuttingPlanes.end();++cpIt)
		if(*cpIt) delete *cpIt;
	}

void medicalViewer::colorMapChangedCallback(Misc::CallbackData* cbData)
	{
	/* Export the changed color map to the palette: */
	paletteEditor->exportColorMap(*palette);

	/* Set the palette in the volume renderer: */
	palette->changeTransparency(transparencyGamma);
	palette->premultiplyAlpha();
	renderer->setColorMap(palette);

	Vrui::requestUpdate();
	}

void medicalViewer::centerDisplayCallback(Misc::CallbackData* cbData)
	{
	/* Calculate initial navigation transformation: */
	Vrui::setNavigationTransformation(Vrui::Point(renderer->getCenter()),Vrui::Scalar(renderer->getRadius()));
	}
void medicalViewer::showPaletteEditorCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData)
	{
	/* Hide or show palette editor based on toggle button state: */
	if(cbData->set)
		{
		/* Pop up the palette editor at the same position as the main menu: */
		Vrui::getWidgetManager()->popupPrimaryWidget(paletteEditor,Vrui::getWidgetManager()->calcWidgetTransformation(mainMenu));
		}
	else
		Vrui::popdownPrimaryWidget(paletteEditor);
	}

void medicalViewer::savePaletteCallback(Misc::CallbackData* cbData)
	{
	paletteEditor->savePalette("Palette.pal");
	}

void medicalViewer::showRenderSettingsDialogCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData)
	{
	/* Hide or show render settings dialog based on toggle button state: */
	if(cbData->set)
		{
		/* Pop up the render settings dialog at the same position as the main menu: */
		Vrui::getWidgetManager()->popupPrimaryWidget(renderSettingsDialog,Vrui::getWidgetManager()->calcWidgetTransformation(mainMenu));
		}
	else
		Vrui::popdownPrimaryWidget(renderSettingsDialog);
	}

void medicalViewer::createInputDeviceCallback(Misc::CallbackData* cbData)
	{
	Vrui::addVirtualInputDevice("Virtual",1,0);
	}

void medicalViewer::saveViewCallback(Misc::CallbackData* cbData)
	{
	/* Open viewpoint file: */
	Misc::File viewpointFile("Viewpoint.dat","wb",Misc::File::LittleEndian);

	/* Get the current navigation transformation: */
	const Vrui::NavTransform& nt=Vrui::getNavigationTransformation();

	/* Write the navigation transformation: */
	viewpointFile.write(nt.getTranslation().getComponents(),3);
	viewpointFile.write(nt.getRotation().getQuaternion(),4);
	viewpointFile.write(nt.getScaling());
	}

void medicalViewer::loadViewCallback(Misc::CallbackData* cbData)
	{
	/* Open viewpoint file: */
	Misc::File viewpointFile("Viewpoint.dat","rb",Misc::File::LittleEndian);

	/* Read the navigation transformation: */
	Vrui::NavTransform::Vector translation;
	viewpointFile.read(translation.getComponents(),3);
	Vrui::NavTransform::Rotation::Scalar quaternion[4];
	viewpointFile.read(quaternion,4);
	Vrui::NavTransform::Scalar scaling=viewpointFile.read<Vrui::NavTransform::Scalar>();

	/* Set the navigation transformation: */
	Vrui::setNavigationTransformation(Vrui::NavTransform(translation,Vrui::NavTransform::Rotation::fromQuaternion(quaternion),scaling));
	}

void medicalViewer::toolCreationCallback(Vrui::ToolManager::ToolCreationCallbackData* cbData)
	{
	/* Call the base class method: */
	Vrui::Application::toolCreationCallback(cbData);
	
	/* Check if the new tool is a surface navigation tool: */
	Vrui::SurfaceNavigationTool* surfaceNavigationTool=dynamic_cast<Vrui::SurfaceNavigationTool*>(cbData->tool);
	if(surfaceNavigationTool!=0)
		{
		/* Set the new tool's alignment function: */
		surfaceNavigationTool->setAlignFunction(Misc::createFunctionCall(this,&medicalViewer::alignSurfaceFrame));
		}

	/* Check if the new tool is a locator tool: */
	Vrui::LocatorTool* locatorTool=dynamic_cast<Vrui::LocatorTool*>(cbData->tool);
	if(locatorTool!=0)
		{
		/* Check if OpenGL has another clipping plane available: */
		for(int i=0;i<numClipPlanes;++i)
			if(!clipPlaneAllocateds[i])
				{
				/* Create a cutting plane locator object and associate it with the new tool: */
				CuttingPlaneLocator* newLocator=new CuttingPlaneLocator(locatorTool,i);

				/* Mark the OpenGL clipping plane as allocated: */
				clipPlaneAllocateds[i]=true;

				/* Add new locator to list: */
				cuttingPlanes.push_back(newLocator);

				break;
				}
		}
	}

void medicalViewer::printUsage()
{
	std::cout << USAGE_STRING << std::endl;
}

void medicalViewer::frame(void)
	{
	#if 0
	
	/* Trace the test box from its current position to the center of the display: */
	PolygonModel::Box testBox;
	// testBoxCenter=PolygonModel::Point(6.75,1.25,-8.26500034+0.5);
	for(int i=0;i<3;++i)
		{
		testBox.min[i]=testBoxCenter[i]-PolygonModel::Scalar(0.5);
		testBox.max[i]=testBoxCenter[i]+PolygonModel::Scalar(0.5);
		}
	
	PolygonModel::Vector displacement=PolygonModel::Point(Vrui::getInverseNavigationTransformation().transform(Vrui::getDisplayCenter()))-testBoxCenter;
	// displacement=PolygonModel::Vector(0.0,0.0,-1.0);
	PolygonModel::Vector hitNormal;
	PolygonModel::Scalar lambda=model->traceBox(testBox,displacement,hitNormal);
	
	if(lambda<PolygonModel::Scalar(1))
		{
		lambda-=PolygonModel::Scalar(0.001)/displacement.mag();
		if(lambda<PolygonModel::Scalar(0.0))
			lambda=PolygonModel::Scalar(0.0);
		testBoxCenter+=displacement*lambda;
		
		/* Let it slide: */
		for(int i=0;i<3;++i)
			{
			testBox.min[i]=testBoxCenter[i]-PolygonModel::Scalar(0.5);
			testBox.max[i]=testBoxCenter[i]+PolygonModel::Scalar(0.5);
			}
		displacement*=(PolygonModel::Scalar(1)-lambda);
		displacement.orthogonalize(hitNormal);
		lambda=model->traceBox(testBox,displacement,hitNormal);
		
		if(lambda<PolygonModel::Scalar(1))
			{
			lambda-=PolygonModel::Scalar(0.001)/displacement.mag();
			if(lambda<PolygonModel::Scalar(0.0))
				lambda=PolygonModel::Scalar(0.0);
			testBoxCenter+=displacement*lambda;
			}
		else
			testBoxCenter+=displacement;
		}
	else
		testBoxCenter+=displacement;
	
	std::cout<<testBoxCenter<<std::endl;
	
	#endif
	if(volumeLoaded)
	{
		/* Retrieve current viewing direction: */
			viewDirection=renderer->getCenter()-VolumeRenderer::Point(Vrui::getHeadPosition());
			viewDirection.normalize();
	}

	if(rotateModels)
	{
		rotationAngle += 2;
		if(rotationAngle>=360.0f)
				rotationAngle-=360.0f;

		/* Request another frame: */
		Vrui::scheduleUpdate(Vrui::getApplicationTime()+1.0/125.0);
	}
	}

void medicalViewer::display(GLContextData& contextData) const
	{
	glPushAttrib(GL_ENABLE_BIT|GL_LIGHTING_BIT|GL_LINE_BIT|GL_POLYGON_BIT);
	glDisable(GL_COLOR_MATERIAL);
	glMaterial(GLMaterialEnums::FRONT_AND_BACK,GLMaterial(GLMaterial::Color(0.6f,0.6f,0.6f),GLMaterial::Color(0.5f,0.5f,0.5f),25.0f));

	/* Rotate all 3D models by the Earth rotation angle: */
	glPushMatrix();
	glRotatef(rotationAngle,rotationAxis[0],rotationAxis[1],rotationAxis[2]);

	if(modelLoaded && showBackfaces)
		{
		/* Disable backface culling and enable two-sided lighting: */
		glDisable(GL_CULL_FACE);
		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);
		}
	
	// Render DICOM images without blending
	if(dicomVisible && dicomAlpha==255)
	{
		// Create the OpenGL texture from RGB DICOM image
		GLuint texture = SOIL_create_OGL_texture(
			(unsigned char*)image->GetBufferPointer(),
			columnCount,
			rowCount,
			SOIL_LOAD_RGBA,
			1,
			SOIL_FLAG_MIPMAPS
		);

		/* check for an error during the load process */
		if( 0 == texture )
		{
			printf( "SOIL error: '%s'\n", SOIL_last_result() );
		}


		/* Draw the mesh's bounding box: */
		glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
		glDisable(GL_LIGHTING);
		glDisable(GL_CULL_FACE);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texture);

		glBegin(GL_QUADS); //Start drawing a Quad
		  glColor3f(1.0f,1.0f,1.0f); //Set the colour to Red
		  //glNormal3f( 0.0f, 1.0f, 0.0f);

		  glTexCoord2f(1.0f, 1.0f);
		  glVertex3f(rlc.x,-rlc.y,-rlc.z);
		  glTexCoord2f(0.0f, 1.0f);
		  glVertex3f(luc.x,-rlc.y,-rlc.z);
		  glTexCoord2f(0.0f, 0.0f);
		  glVertex3f(luc.x,-luc.y,-luc.z);
		  glTexCoord2f(1.0f, 0.0f);
		  glVertex3f(rlc.x,-luc.y,-luc.z);
		glEnd();

		glPopAttrib();
	}

	if(modelLoaded) model->glRenderAction(contextData);
	
	if(modelLoaded && subMesh!=0)
		{
		/* Check if the model is a hierarchical triangle set or contains one: */
		const HierarchicalTriangleSetBase* hts=dynamic_cast<const HierarchicalTriangleSetBase*>(model);
		if(hts==0)
			{
			const MultiModel* mm=dynamic_cast<MultiModel*>(model);
			if(mm!=0)
				hts=mm->getHierarchicalTriangleSet();
			}
		
		//COMMENTED SELECTION HERE
		//glMaterial(GLMaterialEnums::FRONT_AND_BACK,GLMaterial(GLMaterial::Color(1.0f,0.5f,0.5f)));
		//hts->drawSubMesh(*subMesh,contextData);
		}

	// Render DICOM images with blending enabled
	// (Rendered after models)
	if(dicomVisible && dicomAlpha!=255)
	{
		/* load an image file directly as a new OpenGL texture */
		//GLuint texture = SOIL_load_OGL_texture("vabaduse_valjak.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);

		// Create the OpenGL texture from RGB DICOM image
		GLuint texture = SOIL_create_OGL_texture(
			(unsigned char*)image->GetBufferPointer(),
			columnCount,
			rowCount,
			SOIL_LOAD_RGBA,
			1,
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_MULTIPLY_ALPHA
		);

		/* check for an error during the load process */
		if( 0 == texture )
		{
			printf( "SOIL error: '%s'\n", SOIL_last_result() );
		}


		/* Draw the mesh's bounding box: */
		glPushAttrib(GL_ENABLE_BIT|GL_LINE_BIT);
		glDisable(GL_LIGHTING);
		glDisable(GL_CULL_FACE);

		glEnable(GL_BLEND);
		glDepthMask(GL_FALSE);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texture);

		glBegin(GL_QUADS); //Start drawing a Quad
		  glColor3f(1.0f,1.0f,1.0f); //Set the colour to Red
		  //glNormal3f( 0.0f, 1.0f, 0.0f);

		  glTexCoord2f(1.0f, 1.0f);
		  glVertex3f(rlc.x,-rlc.y,-rlc.z);
		  glTexCoord2f(0.0f, 1.0f);
		  glVertex3f(luc.x,-rlc.y,-rlc.z);
		  glTexCoord2f(0.0f, 0.0f);
		  glVertex3f(luc.x,-luc.y,-luc.z);
		  glTexCoord2f(1.0f, 0.0f);
		  glVertex3f(rlc.x,-luc.y,-luc.z);
		glEnd();

		/* Disable blending: */
		glDepthMask(GL_TRUE);
		glDisable(GL_BLEND);

		glPopAttrib();
	}

	if(volumeLoaded)
		{
		/* Enable all cutting planes: */
		for(std::vector<CuttingPlaneLocator*>::const_iterator cpIt=cuttingPlanes.begin();cpIt!=cuttingPlanes.end();++cpIt)
			(*cpIt)->setGLState();

		/* Enable alpha test: */
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER,0.0f);

		/* Draw the volume: */
		renderer->renderBlock(contextData,viewDirection);

		glDisable(GL_ALPHA_TEST);

		/* Disable all cutting planes: */
		for(std::vector<CuttingPlaneLocator*>::const_iterator cpIt=cuttingPlanes.begin();cpIt!=cuttingPlanes.end();++cpIt)
			(*cpIt)->resetGLState();
		}

	#if 0
	
	glDisable(GL_LIGHTING);
	glLineWidth(1.0f);
	glColor3f(0.0f,1.0f,0.0f);
	glBegin(GL_LINE_STRIP);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]+0.5f);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]+0.5f);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]+0.5f);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]+0.5f);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]+0.5f);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]-0.5f,testBoxCenter[2]+0.5f);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]+0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]+0.5f);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]-0.5f);
	glVertex3f(testBoxCenter[0]-0.5f,testBoxCenter[1]+0.5f,testBoxCenter[2]+0.5f);
	glEnd();
	
	#endif
	glPopMatrix();
	glPopAttrib();
	}

void medicalViewer::resetNavigationCallback(Misc::CallbackData* cbData)
	{
		if(dicomLoaded)
		{
			/* Initialize the navigation transformation: */
			dicomReader::point p1 = dicom->getImagePlaneUpperLeftCornerCoordinates(0);
			dicomReader::point p2 = dicom->getImagePlaneLowerRightCornerCoordinates(dicom->getSlicesNumber()-1);
			Geometry::Point<double,3> pp1(p1.x,p1.y,p1.z);
			Geometry::Point<double,3> pp2(p2.x,p2.y,p2.z);
			Vrui::setNavigationTransformation(Geometry::mid(pp1,pp2),Geometry::dist(pp1,pp2),upVector);
			Geometry::Point<double,3> midPoint = Geometry::mid(pp1,pp2);
			rotationAxis[0]=midPoint[0];
			rotationAxis[1]=midPoint[1];
			rotationAxis[2]=midPoint[2];
		}
		if(modelLoaded)
		{
			/* Initialize the navigation transformation: */
			PolygonModel::Box bbox=model->calcBoundingBox();
			Vrui::setNavigationTransformation(Geometry::mid(bbox.min,bbox.max),Geometry::dist(bbox.min,bbox.max),upVector);
			Geometry::Point<double,3> midPoint = Geometry::mid(bbox.min,bbox.max);
			rotationAxis[0]=midPoint[0];
			rotationAxis[1]=midPoint[1];
			rotationAxis[2]=midPoint[2];
		}
	}

void medicalViewer::showBackfacesCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData)
	{
	if(modelLoaded)
	{
		showBackfaces=cbData->set;
	}
	}

void medicalViewer::meshVisibilityCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData)
	{
	if(modelLoaded)
	{
		if(this->subMesh)
		{
			// Set the visibility of the mesh
			mutSubMesh->setVisibility(cbData->set);
			// Update the view in the meshVisibilityDialog
			if(meshesVisibilityDialog!=0) updateMeshesVisibilityDialog();
		}
	}
	}

void medicalViewer::meshesVisibilityCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData)
	{
	if(modelLoaded)
	{
		HierarchicalTriangleSetBase::SubMesh* mesh=0;

		HierarchicalTriangleSetBase* htsa=dynamic_cast<HierarchicalTriangleSetBase*>(model);
		mesh = htsa->findSubMeshByIndex(atoi(cbData->toggle->getName()));

		if(mesh)
		{
			// Set the given mesh visibility
			mesh->setVisibility(cbData->set);
			// Update the submesh dialog if open
			if(subMeshDialog) updateSubMeshDialog();
		}
	}
	}

void medicalViewer::dicomVisibilityCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData)
	{
		dicomVisible = cbData->set;
	}

void medicalViewer::levelModelCallback(Misc::CallbackData* cbData)
	{
	if(modelLoaded)
	{
		/* Rotate such that the up vector and Vrui's up direction are aligned: */
		Vrui::NavTransform nav=Vrui::getNavigationTransformation();
		Vrui::Vector physUp=nav.transform(upVector);
		nav.leftMultiply(Vrui::NavTransform::translateFromOriginTo(Vrui::getDisplayCenter()));
		nav.leftMultiply(Vrui::NavTransform::rotate(Vrui::Rotation::rotateFromTo(physUp,Vrui::getUpDirection())));
		nav.leftMultiply(Vrui::NavTransform::translateToOriginFrom(Vrui::getDisplayCenter()));
		Vrui::setNavigationTransformation(nav);
	}
	}

void medicalViewer::selectMeshCallback(GLMotif::Button::CallbackData* cbData)
	{
	if(modelLoaded)
	{
		/* DEBUG String */
		//std::cout << "Button name selected: " << atoi(cbData->button->getName()) << std::endl;

		/* Check if the model is a hierarchical triangle set or contains one: */
		const HierarchicalTriangleSetBase* hts=dynamic_cast<HierarchicalTriangleSetBase*>(model);
		if(hts==0)
			{
			MultiModel* mm=dynamic_cast<MultiModel*>(model);
			if(mm!=0)
				hts=mm->getHierarchicalTriangleSet();
			}
		if(hts!=0)
			{
				// Find all the submeshes: *
				HierarchicalTriangleSetBase::SubMeshesVectorPointersType meshes;
				meshes = hts->getSubMeshesPointers();
				this->subMesh = meshes[atoi(cbData->button->getName())];
				selectedMesh = atoi(cbData->button->getName())-1;
				// MODIFIED HERE: Added the two following code lines
				HierarchicalTriangleSetBase* htsa=dynamic_cast<HierarchicalTriangleSetBase*>(model);
				if(subMesh) this->mutSubMesh = htsa->findSubMeshByIndex(subMesh->getMeshIndex());
			}
		else
		{
			this->subMesh=0;
			selectedMesh = -1;
		}

		/* Update the application's submesh data dialog: */
		this->updateSubMeshDialog();
	}
	}

void medicalViewer::scaleOneToOneCallback(Misc::CallbackData* cbData)
	{
	if(modelLoaded)
	{
		/* Calculate the correct scaling factor: */
		Vrui::Scalar newNavScale;
		const Geometry::LinearUnit& unit=Vrui::getCoordinateManager()->getUnit();
		if(unit.isImperial())
			{
			/* Calculate scale factor through imperial units: */
			newNavScale=Vrui::getInchFactor()/unit.getInchFactor();
			}
		else
			{
			/* Calculate scale factor through metric units: */
			newNavScale=Vrui::getMeterFactor()/unit.getMeterFactor();
			}

		/* Pre-multiply the navigation transformation with a scaling around the current display center: */
		Vrui::NavTransform nav=Vrui::getNavigationTransformation();
		nav.leftMultiply(Vrui::NavTransform::scaleAround(Vrui::getDisplayCenter(),newNavScale/nav.getScaling()));

		/* Set the new navigation transformation: */
		Vrui::setNavigationTransformation(nav);
	}
	}

void medicalViewer::meshesVisibilityDialogCallback(Misc::CallbackData* cbData)
	{
	if(modelLoaded)
	{
		/* Create and open the submesh data dialog if it doesn't exist: */
		if(!meshesVisibilityDialog)
		{
			meshesVisibilityDialog=createMeshesVisibilityDialog();
			Vrui::popupPrimaryWidget(meshesVisibilityDialog);
		}
	}
	}

void medicalViewer::dicomDialogCallback(Misc::CallbackData* cbData)
	{
		/* Create and open the submesh data dialog if it doesn't exist: */
		if(!dicomDialog)
		{
			dicomDialog=createDicomDialog();
			Vrui::popupPrimaryWidget(dicomDialog);
		}
	}

void medicalViewer::meshesVisibilityDialogCloseCallback(Misc::CallbackData* cbData)
	{
	if(modelLoaded)
	{
		/* Delete the mesh visibility data dialog: */
		Vrui::getWidgetManager()->deleteWidget(meshesVisibilityDialog);
		meshesVisibilityDialog=0;
		if(!meshesVisibilityButtons.empty()) meshesVisibilityButtons.clear();
	}
	}

void medicalViewer::dicomDialogCloseCallback(Misc::CallbackData* cbData)
	{
		if(dicomSliceSliderLabel) delete(dicomSliceSliderLabel);
		/* Delete the DICOM dialog: */
		Vrui::getWidgetManager()->deleteWidget(dicomDialog);
		dicomDialog=0;
	}

void medicalViewer::subMeshDialogCloseCallback(Misc::CallbackData* cbData)
	{
	if(modelLoaded)
	{
		/* Delete the submesh data dialog: */
		Vrui::getWidgetManager()->deleteWidget(subMeshDialog);
		subMeshDialog=0;
	}
	}

void medicalViewer::updateSubMeshDialog(void)
	{
	if(modelLoaded && subMesh!=0)
		{
		/* Create and open the submesh data dialog if it doesn't exist: */
		if(subMeshDialog==0)
			{
			subMeshDialog=createSubMeshDialog();
			Vrui::popupPrimaryWidget(subMeshDialog);
			}

		/* Update the submesh data: */
		nameField->setString(subMesh->getName().c_str());
		meshVisibilityButton->setToggle(subMesh->getVisibility());


		bool enableTransparencySlider = true;
		for(int i=0;i<loadedMeshFiles.size();i++)
		{
			if(loadedMeshFiles[i].isTransparent && i!=selectedMesh)
				enableTransparencySlider = false;

			if(dicomAlpha != 255 || volumeLoaded) enableTransparencySlider = false;
		}
		// Update the slider value
		typedef Misc::Autopointer<PhongMaterial> MaterialPointer;
		const MaterialPointer mat = static_cast<MaterialPointer>(mutSubMesh->getMaterial());
		GLMaterial oldMat = mat->getFrontMaterial();
		if(enableTransparencySlider)
		{
			surfaceTransparencySlider->setValueRange(0.0,1.0,0.02);
			surfaceTransparencySlider->setValue(oldMat.diffuse[3]);
		}
		else
		{
			surfaceTransparencySlider->setValueRange(1,1,1);
		}


		numTrianglesField->setValue((unsigned int)(subMesh->getNumTriangles()));
		PolygonModel::Box bbox=subMesh->getBoundingBox();
		for(int i=0;i<3;++i)
			{
			bboxField[0+i]->setValue(bbox.min[i]);
			bboxField[3+i]->setValue(bbox.max[i]);
			}
		for(int i=0;i<3;++i)
			bboxCenterField[i]->setValue(Math::mid(bbox.min[i],bbox.max[i]));
		}
	else if(subMeshDialog!=0)
		{
		/* Reset the submesh data dialog: */
		nameField->setString("");
		numTrianglesField->setString("");
		for(int i=0;i<6;++i)
			bboxField[i]->setString("");
		}
	}

void medicalViewer::updateMeshesVisibilityDialog(void)
	{
		if(modelLoaded && meshesVisibilityDialog!=0)
		{
			/* Check if the model is a hierarchical triangle set or contains one: */
			const HierarchicalTriangleSetBase* hts=dynamic_cast<HierarchicalTriangleSetBase*>(model);
			if(hts==0)
				{
				MultiModel* mm=dynamic_cast<MultiModel*>(model);
				if(mm!=0)
					hts=mm->getHierarchicalTriangleSet();
				}
			if(hts!=0)
				{
					// Find all the submeshes: *
					HierarchicalTriangleSetBase::SubMeshesVectorPointersType meshes;
					meshes = hts->getSubMeshesPointers();
					if(!meshesVisibilityButtons.empty())
					{
						// Iterate over toggle buttons and update with the current meshes visibility settings
						for(int i=1;i<meshes.size();i++)
						{
							meshesVisibilityButtons[i-1]->setToggle(meshes[i]->getVisibility());
						}
					}
				}
			else
				subMesh=0;
		}
	}

void medicalViewer::updateDicomDialog(void)
	{
		/* Create and open the submesh data dialog if it doesn't exist: */
		if(dicomDialog==0)
			{
			dicomDialog=createDicomDialog();
			Vrui::popupPrimaryWidget(dicomDialog);
			}

		// Set the slice number
		char SliceIndexText[20];
		snprintf(SliceIndexText,sizeof(SliceIndexText),"Slice [ %d / %d ]",sliceNumber+1,dicom->getSlicesNumber());
		dicomSliceSliderLabel->setString(SliceIndexText);

		// Set the various values
		dicomVisibilityButton->setToggle(dicomVisible);

		//dicomTransparencySlider->setValueRange(0.0,1.0,0.001);
		dicomSliceSlider->setValue(sliceNumber);
		dicomLowerLevelSlider->setValue(lowerThreshold);
		dicomUpperLevelSlider->setValue(upperThreshold);
	}

void medicalViewer::alignSurfaceFrame(Vrui::SurfaceNavigationTool::AlignmentData& alignmentData)
	{
	if(modelLoaded)
	{
		typedef PolygonModel::Scalar Scalar;
		typedef PolygonModel::Point Point;
		typedef PolygonModel::Vector Vector;
		typedef PolygonModel::Box Box;

		/* Calculate the player's current head height: */
		Vrui::Point headPosPhys=Vrui::getMainViewer()->getHeadPosition();
		const Vrui::Vector& floorNormal=Vrui::getFloorPlane().getNormal();
		Vrui::Scalar floorLambda=(Vrui::getFloorPlane().getOffset()-headPosPhys*floorNormal)/(Vrui::getUpDirection()*floorNormal);
		Vrui::Point footPosPhys=headPosPhys+Vrui::getUpDirection()*floorLambda;
		Vrui::Scalar playerHeightPhys=Geometry::dist(headPosPhys,footPosPhys);
		Scalar playerHeight=Scalar(playerHeightPhys*alignmentData.surfaceFrame.getScaling());

		/* Extract the new surface frame's base point: */
		Vrui::Point base=alignmentData.surfaceFrame.getOrigin();
		Point foot(base);

		/* Create a local coordinate frame to account for upVector != (0,0,1): */
		Vrui::Rotation localFrame=Vrui::Rotation::rotateFromTo(Vrui::Vector(0,0,1),upVector);

		/* Initialize the alignment state if it doesn't already exist: */
		AlignmentState* as=static_cast<AlignmentState*>(alignmentData.alignmentState);
		if(as==0)
			{
			/* Create a new alignment state object: */
			alignmentData.alignmentState=as=new AlignmentState;
			as->set(playerHeight,foot,alignmentData.probeSize);
			}

		/* Raise the player's foot and transform the player box to model coordinates: */
		Box player=as->playerBox;
		player.min[2]+=alignmentData.maxClimb;
		player.transform(Vrui::ONTransform::rotateAround(Vrui::Point(as->playerFoot),localFrame));

		/* Trace the player box from its old to its new position: */
		Vector displacement=foot-as->playerFoot;
		Vector hitNormal;
		Scalar lambda=model->traceBox(player,displacement,hitNormal);
		Point newFoot=as->playerFoot+displacement*lambda;

		/* Check if the box hit something: */
		if(lambda<Scalar(1))
			{
			#if 1

			/* Move the box to its intermediate position: */
			as->set(playerHeight,newFoot,alignmentData.probeSize);

			/* Raise the player's foot and transform the player box to model coordinates: */
			player=as->playerBox;
			player.min[2]+=alignmentData.maxClimb;
			player.transform(Vrui::ONTransform::rotateAround(Vrui::Point(as->playerFoot),localFrame));

			/* Slide along the hit plane towards the intended end position: */
			Vector slideDisplacement=displacement*(Scalar(1)-lambda);
			slideDisplacement.orthogonalize(hitNormal);
			if(displacement*hitNormal>Scalar(0))
				slideDisplacement-=hitNormal*(epsilon/hitNormal.mag());
			else
				slideDisplacement+=hitNormal*(epsilon/hitNormal.mag());
			lambda=model->traceBox(player,slideDisplacement,hitNormal);
			newFoot=as->playerFoot+slideDisplacement*lambda;

			#endif
			}
		
		/* Move the box to its end position: */
		as->set(playerHeight,newFoot,alignmentData.probeSize);
		
		#if 1

		/* Drop the player's foot back to the floor: */
		player=as->playerBox;
		player.min[2]+=alignmentData.maxClimb;
		player.transform(Vrui::ONTransform::rotateAround(Vrui::Point(as->playerFoot),localFrame));
		displacement=upVector*(-alignmentData.maxClimb*Scalar(4));
		lambda=model->traceBox(player,displacement,hitNormal);

		if(lambda<Scalar(1))
			{
			/* The player stepped on something; raise the head instead: */
			// ...
			}
		
		/* Move the box to its final position: */
		as->set(playerHeight,as->playerFoot+upVector*alignmentData.maxClimb+displacement*lambda,alignmentData.probeSize);
		
		#endif

		/* Update the passed frame: */
		alignmentData.surfaceFrame=Vrui::NavTransform(Vrui::Point(as->playerFoot)-Vrui::Point::origin,localFrame,alignmentData.surfaceFrame.getScaling());
	}
	}

VRUI_APPLICATION_RUN(medicalViewer)
