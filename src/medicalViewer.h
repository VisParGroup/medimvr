/***********************************************************************
medicalViewer - Vrui application. Based on the Oliver Kreylos's
Polyghon Mesh Text class
***********************************************************************/

#ifndef MEDICAL_VIEWER
#define MEDICAL_VIEWER

#include <Geometry/Point.h>
#include <Geometry/Vector.h>
#include <Geometry/OrthogonalTransformation.h>
#include <GLMotif/ToggleButton.h>
#include <GLMotif/Button.h>
#include <GLMotif/CascadeButton.h>
#include <GLMotif/Menu.h>
#include <GLMotif/SubMenu.h>
#include <GLMotif/Slider.h>
#include <GLMotif/StyleSheet.h>
#include <Vrui/Geometry.h>
#include <Vrui/Tool.h>
#include <Vrui/GenericToolFactory.h>
#include <Vrui/SurfaceNavigationTool.h>
#include <Vrui/TransformTool.h>
#include <Vrui/LocatorTool.h>
#include <Vrui/LocatorToolAdapter.h>
#include <Vrui/Application.h>
#include <Misc/SelfDestructPointer.h>
#include <Misc/StringPrintf.h>
#include <Misc/ThrowStdErr.h>
#include <Misc/StringHashFunctions.h>
#include <Misc/HashTable.h>
#include <GL/gl.h>
#include <GL/GLObject.h>
#include <SOIL/SOIL.h>

#include <dirent.h>
#include <string.h>

#include "PolygonModel.h"
#include "HierarchicalTriangleSetBase.h"
#include "MeshVertex.h"
#include "Material.h"
#include "PhongMaterial.h"
#include "./dicom/dicomReader.h"
#include "PaletteEditor.h"
#include "PaletteRenderer.h"

#define USAGE_STRING "Usage: medicalV [OPTIONS]... [MODELS]... [-d DIRECTORY...]... [-dd DICOM_DIRECTORY [-cv [filename]]] [-v volume]...\n\\
Show medical model(s) given as MODELS(s) in OBJ, ASE, LWS, LWO and PLY  format.\n\n\\
The program can also:\n\\
	1) load model files in the above supported formats from directories names\n\\
	   given after -d switch\n\\
	2) load a DICOM_DIRECTORY of dicom files directory with the -dd option\n\\
	   specified and convert them to a \".vol\" volume file with optional -cv\n\\
	   option specified. The filename parameter is optional. If not used\n\\
	   the output volume will be names as DICOM_DIRECTORY.vol\n\\
	3) load a volume file in \".vol\", \".nrrd\" and \".nhdr\" format\n\n\\
The following options can also be specified:\n\\
	-up x y z: define an up vector with (x,y,z) float coordinates\n\\
	-p filename: load a previous saved \"filename\" palette\n\\
	-h --HELP: print this help\n"

/* Forward declarations: */
namespace GLMotif {
class PopupMenu;
class PopupWindow;
class TextField;
}
class MaterialManager;

class medicalViewer:public Vrui::Application
	{
	/* Embedded classes: */
	private:
	class ModelProbeTool;
	typedef Vrui::GenericToolFactory<ModelProbeTool> ModelProbeToolFactory; // Tool class uses the generic factory class
	
	class ModelProbeTool:public Vrui::Tool,public Vrui::Application::Tool<medicalViewer>
		{
		friend class Vrui::GenericToolFactory<ModelProbeTool>;
		
		/* Elements: */
		private:
		static ModelProbeToolFactory* factory; // Pointer to the factory object for this class
		
		bool active;
		bool haveIntersection;
		Vrui::Point intersection;
		
		/* Constructors and destructors: */
		public:
		ModelProbeTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment);
		
		/* Methods from Vrui::Tool: */
		virtual const Vrui::ToolFactory* getFactory(void) const
			{
			return factory;
			}
		virtual void buttonCallback(int buttonSlotIndex,Vrui::InputDevice::ButtonCallbackData* cbData);
		virtual void frame(void);
		virtual void display(GLContextData& contextData) const;
		};
	
	class RotationTool;
	typedef Vrui::GenericToolFactory<RotationTool> RotationToolFactory; // Tool class uses the generic factory class

	class RotationTool:public Vrui::Tool,public Vrui::Application::Tool<medicalViewer>
		{
		friend class Vrui::GenericToolFactory<RotationTool>;

		/* Elements: */
		private:
		static RotationToolFactory* factory; // Pointer to the factory object for this class

		/* Constructors and destructors: */
		public:
		RotationTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment);

		/* Methods from Vrui::Tool: */
		virtual const Vrui::ToolFactory* getFactory(void) const
			{
			return factory;
			}
		virtual void buttonCallback(int buttonSlotIndex,Vrui::InputDevice::ButtonCallbackData* cbData);
		};

	class ChangeSliceTool;
	typedef Vrui::GenericToolFactory<ChangeSliceTool> ChangeSliceToolFactory; // Tool class uses the generic factory class

	class ChangeSliceTool:public Vrui::TransformTool,public Vrui::Application::Tool<medicalViewer>
		{
		friend class Vrui::GenericToolFactory<ChangeSliceTool>;

		/* Elements: */
		private:
		static ChangeSliceToolFactory* factory; // Pointer to the factory object for this class
		bool moveUp,moveDown;

		/* Constructors and destructors: */
		public:
		ChangeSliceTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment);

		/* Methods from Vrui::Tool: */
		virtual const Vrui::ToolFactory* getFactory(void) const
			{
			return factory;
			}
		virtual void buttonCallback(int buttonSlotIndex,Vrui::InputDevice::ButtonCallbackData* cbData);
		//virtual void valuatorCallback(int valuatorSlotIndex,Vrui::InputDevice::ValuatorCallbackData* cbData);
		virtual void frame(void);
		};

	class ModelProjectorTool;
	typedef Vrui::GenericToolFactory<ModelProjectorTool> ModelProjectorToolFactory; // Tool class uses the generic factory class
	
	class ModelProjectorTool:public Vrui::TransformTool,public Vrui::Application::Tool<medicalViewer>
		{
		friend class Vrui::GenericToolFactory<ModelProjectorTool>;
		
		/* Elements: */
		private:
		static ModelProjectorToolFactory* factory; // Pointer to the factory object for this class
		
		/* Constructors and destructors: */
		public:
		ModelProjectorTool(const Vrui::ToolFactory* factory,const Vrui::ToolInputAssignment& inputAssignment);
		
		/* Methods from Vrui::Tool: */
		virtual void initialize(void);
		virtual const Vrui::ToolFactory* getFactory(void) const
			{
			return factory;
			}
		virtual void frame(void);
		};
	
	class AlignmentState:public Vrui::SurfaceNavigationTool::AlignmentState // Application data to be retained between alignSurfaceFrame calls
		{
		/* Elements: */
		public:
		PolygonModel::Scalar playerHeight; // Total height of player from eye to foot
		PolygonModel::Point playerFoot; // Foot position of player
		PolygonModel::Box playerBox; // Bounding box enclosing player "avatar" in z-is-up model coordinates
		
		/* Methods: */
		void set(PolygonModel::Scalar newPlayerHeight,const PolygonModel::Point& newPlayerFoot,PolygonModel::Scalar playerRadius); // Updates the alignment state with new player parameters
		};
	
	/* Embedded class of Volume Renderer: */
	private:
	class CuttingPlaneLocator:public Vrui::LocatorToolAdapter // Class to create and manipulate cutting planes
		{
		/* Elements: */
		private:
		GLuint clipPlaneIndex; // Index of OpenGL clip plane used for this tool
		bool active; // Flag whether the cutting plane is enabled
		Vrui::Vector planeNormal; // Normal vector of the cutting plane
		Vrui::Scalar planeOffset; // Offset of the cutting plane

		/* Constructors and destructors: */
		public:
		CuttingPlaneLocator(Vrui::LocatorTool* sTool,GLuint sClipPlaneIndex);

		/* Methods: */
		virtual void motionCallback(Vrui::LocatorTool::MotionCallbackData* cbData);
		virtual void buttonPressCallback(Vrui::LocatorTool::ButtonPressCallbackData* cbData);
		virtual void buttonReleaseCallback(Vrui::LocatorTool::ButtonReleaseCallbackData* cbData);

		/* New methods: */
		int getClipPlaneIndex(void) const // Returns the index of the OpenGL clipping plane allocated for the tool
			{
			return clipPlaneIndex;
			};
		void setGLState(void) const; // Sets OpenGL state for cutting plane rendering
		void resetGLState(void) const; // Resets OpenGL state after rendering
		};

	/* Elements: */
	private:
	MaterialManager* materialManager; // A material manager
	PolygonModel* model; // The MODEL!
	Vrui::Vector upVector; // Vector defining the "up" direction in model space
	PolygonModel::Scalar epsilon; // Small fudge value to robustify collision detection inside the model
	bool showBackfaces; // Flag whether to render back-facing polygons
	bool modelLoaded; // Model loaded flag
	bool pUsage;
	const HierarchicalTriangleSetBase::SubMesh* subMesh; // Pointer to highlighted submesh
	HierarchicalTriangleSetBase::SubMesh* mutSubMesh; // Pointer to highlighted submesh
	GLMotif::PopupMenu* mainMenu; // The application's main menu
	GLMotif::PopupWindow* subMeshDialog;
	GLMotif::PopupWindow* meshesVisibilityDialog;
	GLMotif::PopupWindow* dicomDialog;
	GLMotif::TextField* nameField;
	GLMotif::ToggleButton* meshVisibilityButton;
	GLMotif::ToggleButton* dicomVisibilityButton;
	GLMotif::Slider* surfaceTransparencySlider;
	GLMotif::Slider* dicomTransparencySlider;
	GLMotif::Slider* dicomSliceSlider;
	GLMotif::Slider* dicomLowerLevelSlider;
	GLMotif::Slider* dicomUpperLevelSlider;
	GLMotif::Label*  dicomSliceSliderLabel;
	GLMotif::TextField* numTrianglesField;
	GLMotif::TextField* bboxField[6];
	GLMotif::TextField* bboxCenterField[3];
	std::vector<GLMotif::ToggleButton*> meshesVisibilityButtons;

	/* Dicom files related variables */
	// DICOM class pointer
	dicomReader *dicom;
	// Left upper corner (luc) coordinates of the current slice plane
	dicomReader::PointType luc;
	// Right lower corner (rlc) coordinates of the current slice plane
	dicomReader::PointType rlc;
	// Pointer to the image slice
	dicomReader::RGBAImageType::Pointer image;
	// Dimensions (row,col) in pixels^2 of the current slice
	unsigned int rowCount,columnCount;
	// Flag for loading/visibility
	bool dicomLoaded, dicomVisible;
	// Variable that contains the actual slice number
	unsigned int sliceNumber;
	// Variable that contains DICOM conversion levels
	int lowerThreshold, upperThreshold;
	// Keep the number of selected mesh
	int selectedMesh;

	struct filePar {
			unsigned int index;
			bool isValid;
			std::string directory;
			std::string filename;
			std::string extension;
			std::string path;
			bool isTransparent;
			PolygonModel* part;
		};
	std::vector<filePar> loadedMeshFiles;



	/********************** Volume rendering declarations ***************************/
	/********************** Volume rendering declarations ***************************/
	/* Model state: */
	PaletteRenderer* renderer; // Pointer to used volume renderer
	GLColorMap* palette; // Pointer to the used color map
	VolumeRenderer::Scalar sliceFactor; // Number of textured slices to generate per cell
	GLfloat transparencyGamma; // Gamma factor for the color map's transparency component

	/* UI widgets: */
	GLMotif::Popup* volumeRenderingMenu; // Program's main menu
	PaletteEditor* paletteEditor; // Pointer to the color map editor
	GLMotif::PopupWindow* renderSettingsDialog; // Dialog box for rendering settings
	GLMotif::Label* sliceFactorValue; // Text field for slice factor value
	GLMotif::Slider* sliceFactorSlider; // Slider for slice factor value
	GLMotif::Label* transparencyGammaValue; // Text field for transparency gamma factor
	GLMotif::Slider* transparencyGammaSlider; // Slider for transparency gamma factor

	/* Interaction parameters: */
	VolumeRenderer::Vector viewDirection; // Current view direction
	int numClipPlanes; // Number of clipping planes supported by OpenGL
	bool* clipPlaneAllocateds; // Array of allocation flags for the OpenGL clipping planes
	std::vector<CuttingPlaneLocator*> cuttingPlanes; // List of cutting planes

	/* Added variables */
	bool volumeLoaded;
	bool volumeVisible;
	unsigned char dicomAlpha;
	float rotationAngle;
	bool rotateModels;
	Geometry::Vector<double,3> rotationAxis;

	/********************** Volume rendering declarations ***************************/
	/********************** Volume rendering declarations ***************************/





	/* Private methods: */
	GLMotif::PopupMenu*   createMainMenu(void);
	GLMotif::Popup*       createMeshesMenu(void);
	GLMotif::PopupWindow* createSubMeshDialog(void);
	GLMotif::PopupWindow* createMeshesVisibilityDialog(void);
	GLMotif::PopupWindow* createDicomDialog(void);
	GLMotif::Popup*       createVolumeRenderingMenu(void);
	GLMotif::PopupWindow* createRenderSettingsDialog(void);
	
	/* Constructors and destructors: */
	public:
	medicalViewer(int& argc,char**& argv);
	virtual ~medicalViewer(void);
	
	/* Methods from Vrui::Application: */
	virtual void toolCreationCallback(Vrui::ToolManager::ToolCreationCallbackData* cbData);
	virtual void frame(void);
	virtual void display(GLContextData& contextData) const;
	
	/* New methods: */
	void resetNavigationCallback(Misc::CallbackData* cbData);
	void showBackfacesCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void meshVisibilityCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void meshesVisibilityCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void dicomVisibilityCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void levelModelCallback(Misc::CallbackData* cbData);
	void selectMeshCallback(GLMotif::Button::CallbackData* cbData);
	void scaleOneToOneCallback(Misc::CallbackData* cbData);
	void meshesVisibilityDialogCallback(Misc::CallbackData* cbData);
	void dicomDialogCallback(Misc::CallbackData* cbData);
	void subMeshDialogCloseCallback(Misc::CallbackData* cbData);
	void meshesVisibilityDialogCloseCallback(Misc::CallbackData* cbData);
	void dicomDialogCloseCallback(Misc::CallbackData* cbData);
	void updateSubMeshDialog(void);
	void updateMeshesVisibilityDialog(void);
	void updateDicomDialog(void);
	void alignSurfaceFrame(Vrui::SurfaceNavigationTool::AlignmentData& alignmentData);
	void sliderCallback(GLMotif::Slider::ValueChangedCallbackData* cbData);

	void colorMapChangedCallback(Misc::CallbackData* cbData);
	void centerDisplayCallback(Misc::CallbackData* cbData);
	void showPaletteEditorCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void savePaletteCallback(Misc::CallbackData* cbData);
	void showRenderSettingsDialogCallback(GLMotif::ToggleButton::ValueChangedCallbackData* cbData);
	void createInputDeviceCallback(Misc::CallbackData* cbData);
	void saveViewCallback(Misc::CallbackData* cbData);
	void loadViewCallback(Misc::CallbackData* cbData);

	private:
	void printUsage();
	};

#endif
