/*
 * dicomReader.cpp
 *
 *  Created on: Mar 26, 2015
 *      Author: Francesco Ricciardi
 */
#include "dicomReader.h"


dicomReader::dicomReader()
{
	std::cout << "Default Constructor Called" << std::endl;
	//Set to false the dicomLoaded var
	dicomLoaded = false;
	// Null the pointers
	inputNames = NULL;
	gdcmIO = NULL;
	reader = NULL;
	// Set the threshold to default values
	lowerThreshold = DEFAULT_LOWER_THRESHOLD;
	upperThreshold = DEFAULT_UPPER_THRESHOLD;
}

dicomReader::dicomReader(std::string const &dirName)
{
	//Set to false the dicomLoaded var
	dicomLoaded = false;
	dicomDirName = dirName;
	// Null the pointers
	inputNames = NULL;
	gdcmIO = NULL;
	reader = NULL;
	// Set the threshold to default values
	lowerThreshold = DEFAULT_LOWER_THRESHOLD;
	upperThreshold = DEFAULT_UPPER_THRESHOLD;
}

dicomReader::~dicomReader()
{
	std::cout << "Destructor Called" << std::endl;
}

// Set the DICOM directory name
void dicomReader::setDicomDirectory(std::string const &directoryName)
{
	dicomDirName = directoryName;
}

// Trigger the directory reading and file loading
bool dicomReader::readDicomDirectory()
{
	// Check if the directory name is ok
	if(!dicomDirName.size()) return false;

	// Get the DICOM input names
	inputNames = InputNamesGeneratorType::New();
	inputNames->SetUseSeriesDetails( true );
	inputNames->SetInputDirectory(dicomDirName);
	const ReaderType::FileNamesContainer & filenames = inputNames->GetInputFileNames();

	// Instantiate GDCM IO class
	gdcmIO = ImageIOType::New();
	// Instantiate the reader
	reader = ReaderType::New();

	// Try files reading
	reader->SetImageIO( gdcmIO );
	reader->SetFileNames( filenames );
	try
	{
		reader->Update();
	}
	catch (itk::ExceptionObject &excp)
	{
		std::cerr << "Exception thrown while reading the series" << std::endl;
		std::cerr << excp << std::endl;
		return false;
	}

	// Set true the dicomLoaded state
	dicomLoaded = true;
	return true;
}

// Print DICOM tags
void dicomReader::printDicomTags()
{
	if(dicomLoaded)
	{
		typedef itk::MetaDataDictionary   DictionaryType;
		const  DictionaryType & dictionary = gdcmIO->GetMetaDataDictionary();
		typedef itk::MetaDataObject< std::string > MetaDataStringType;
		DictionaryType::ConstIterator itr = dictionary.Begin();
		DictionaryType::ConstIterator end = dictionary.End();

		while( itr != end )
		{
			itk::MetaDataObjectBase::Pointer  entry = itr->second;
			MetaDataStringType::Pointer entryvalue =
			  dynamic_cast<MetaDataStringType *>( entry.GetPointer() );
			if( entryvalue )
			  {
			  std::string tagkey   = itr->first;
			  std::string tagvalue = entryvalue->GetMetaDataObjectValue();
			  std::cout << tagkey <<  " = " << tagvalue << std::endl;
			  }
			++itr;
		}
	}
	else
		std::cout << "printDicomTags() error: No DICOM file loaded" << std::endl;
}

// Read a specific DICOM Tags in the format "0020|0032"
std::string dicomReader::readDicomTag(std::string tagID)
{
	if(dicomLoaded)
	{
		typedef itk::MetaDataDictionary   DictionaryType;
		const  DictionaryType & dictionary = gdcmIO->GetMetaDataDictionary();
		typedef itk::MetaDataObject< std::string > MetaDataStringType;

		// Find the DICOM tag
		DictionaryType::ConstIterator end = dictionary.End();
		DictionaryType::ConstIterator tagItr = dictionary.Find( tagID );

		if( tagItr == end )
		{
			std::cerr << "readDicomTags() warning: Tag " << tagID;
			std::cerr << " not found in the DICOM header" << std::endl;

			std::string emptyString;
			return emptyString;
		}

		MetaDataStringType::ConstPointer entryvalue =
		    dynamic_cast<const MetaDataStringType *>( tagItr->second.GetPointer() );

		if( entryvalue )
		{
			return entryvalue->GetMetaDataObjectValue();
		}
		else
		{
			std::string emptyString;
			return emptyString;
		}
	}
	else
		std::cout << "readDicomTags() error: No DICOM file loaded" << std::endl;
}

// Get the Row spacing in the image matrix in millimeters
// The value 0 represent a not valid spacing (maybe absent spacing tag)
double dicomReader::getRowSpacing()
{
	if(dicomLoaded)
	{
		// Try to get the Row/column spacing TAG
		std::string tagID = readDicomTag("0028|0030");

		// Get the row spacing substring
		std::string rowSpacing = tagID.substr(0, tagID.find("\\"));

		// Check the size of the string
		if(rowSpacing.length())
		{
			return atof(rowSpacing.c_str());
		}
		else
			return 0;
	}
	else
		std::cout << "getRowSpacing() error: No DICOM file loaded" << std::endl;
}
// Get the Column spacing in the image matrix in millimeters
// The value 0 represent a not valid spacing (maybe absent spacing tag)
double dicomReader::getColumnSpacing()
{
	if(dicomLoaded)
	{
		// Try to get the Row/column spacing TAG
		std::string tagID = readDicomTag("0028|0030");

		// Get the col spacing substring
		std::string colSpacing = tagID.substr(tagID.find("\\")+1,tagID.length());

		// Check the size of the string
		if(colSpacing.length())
		{
			return atof(colSpacing.c_str());
		}
		else
			return 0;
	}
	else
		std::cout << "getRowSpacing() error: No DICOM file loaded" << std::endl;
}
// Get the number of row
// The value 0 represent a not valid spacing (maybe absent tag)
unsigned int dicomReader::getRowNumber()
{
	if(dicomLoaded)
	{
		// Try to get the Row/column spacing TAG
		std::string tagID = readDicomTag("0028|0010");

		// Check the size of the string
		if(tagID.length())
		{
			return atoi(tagID.c_str());
		}
		else
			return 0;
	}
	else
		std::cout << "getRowNumber() error: No DICOM file loaded" << std::endl;
}
// Get the number of column
// The value 0 represent a not valid spacing (maybe absent tag)
unsigned int dicomReader::getColumnNumber()
{
	if(dicomLoaded)
	{
		// Try to get the Row/column spacing TAG
		std::string tagID = readDicomTag("0028|0011");

		// Check the size of the string
		if(tagID.length())
		{
			return atoi(tagID.c_str());
		}
		else
			return 0;
	}
	else
		std::cout << "getColumnNumber() error: No DICOM file loaded" << std::endl;
}
// Get the number of slice of the series
unsigned int dicomReader::getSlicesNumber()
{
	if(dicomLoaded)
	{
		// Get the number of slices
		InputImageType::Pointer image;
		image = reader->GetOutput();
		InputImageType::RegionType region = image->GetLargestPossibleRegion();
		return region.GetSize(2);
	}
	else
		std::cout << "getSlicesNumber() error: No DICOM file loaded" << std::endl;
}
// Get the nominal slice thickness in millimeters
// The value 0 represent a not valid spacing (maybe absent tag)
double dicomReader::getSliceThickness()
{
	if(dicomLoaded)
		{
			// Try to get the Row/column spacing TAG
			std::string tagID = readDicomTag("0018|0050");
			// Check the size of the string
			if(tagID.length())
			{
				return atof(tagID.c_str());
			}
			else
				return 0;
		}
		else
			std::cout << "getSliceThickness() error: No DICOM file loaded" << std::endl;
}
// Get the image position i.e. the x,y,z coordinates of the upper left
// hand corner of the image. It is the center of the first volex transmitted.
dicomReader::PointType dicomReader::getImagePosition()
{
	if(dicomLoaded)
	{
		// Try to get the image position TAG
		std::string tagID = readDicomTag("0020|0032");

		if(tagID.length())
		{
			std::string x,y,z;
			std::stringstream stream(tagID);

			getline(stream,x,'\\');
			getline(stream,y,'\\');
			getline(stream,z,'\\');

			PointType point;
			point.x = atof(x.c_str());
			point.y = atof(y.c_str());
			point.z = atof(z.c_str());

			return point;
		}
	}
	else
		std::cout << "getImagePosition() error: No DICOM file loaded" << std::endl;
	// Return a zero point
	PointType zeroPoint;
	zeroPoint.x = zeroPoint.y = zeroPoint.z = 0;
	return zeroPoint;
}
// Get the direction cosine for the rows of the image
dicomReader::VectorType dicomReader::getRowDirectionCosine()
{
	if(dicomLoaded)
	{
		// Try to get direction cosine TAG
		std::string tagID = readDicomTag("0020|0037");

		if(tagID.length())
		{
			std::string x,y,z;
			std::stringstream stream(tagID);

			// Get the row direction cosine
			getline(stream,x,'\\');
			getline(stream,y,'\\');
			getline(stream,z,'\\');

			VectorType point;
			point.x = atof(x.c_str());
			point.y = atof(y.c_str());
			point.z = atof(z.c_str());

			return point;
		}
	}
	else
		std::cout << "getRowDirectionCosine() error: No DICOM file loaded" << std::endl;
	// Return a zero point
	VectorType zeroPoint;
	zeroPoint.x = zeroPoint.y = zeroPoint.z = 0;
	return zeroPoint;
}
// Get the direction cosine for the coulmns of the image
dicomReader::VectorType dicomReader::getColumnDirectionCosine()
{
	if(dicomLoaded)
	{
		// Try to get direction cosine TAG
		std::string tagID = readDicomTag("0020|0037");

		if(tagID.length())
		{
			std::string x,y,z;
			std::stringstream stream(tagID);

			// Dummy reading to remove the row direction cosine
			getline(stream,x,'\\');
			getline(stream,x,'\\');
			getline(stream,x,'\\');
			// Get the column direction cosine
			getline(stream,x,'\\');
			getline(stream,y,'\\');
			getline(stream,z,'\\');

			VectorType point;
			point.x = atof(x.c_str());
			point.y = atof(y.c_str());
			point.z = atof(z.c_str());

			return point;
		}
	}
	else
		std::cout << "getColumnDirectionCosine() error: No DICOM file loaded" << std::endl;
	// Return a zero point
	VectorType zeroPoint;
	zeroPoint.x = zeroPoint.y = zeroPoint.z = 0;
	return zeroPoint;
}
// Get the image position coordinates
dicomReader::PointType dicomReader::getImagePositionCoordinates()
{
	if(dicomLoaded)
	{
		PointType  S;
		S.x = reader->GetOutput()->GetOrigin()[0];
		S.y = reader->GetOutput()->GetOrigin()[1];
		S.z = reader->GetOutput()->GetOrigin()[2];
		VectorType X = getRowDirectionCosine();
		VectorType Y = getColumnDirectionCosine();
		double    di = getRowSpacing();
		double    dj = getColumnSpacing();

		// Compute the point location
		PointType pointLocation;
		pointLocation.x = (-di/2.00)*X.x + (-dj/2.00)*Y.x + S.x;
		pointLocation.y = (-di/2.00)*X.y + (-dj/2.00)*Y.y + S.y;
		pointLocation.z = (-di/2.00)*X.z + (-dj/2.00)*Y.z + S.z;

		return pointLocation;
	}
	else
		std::cout << "getImagePlaneUpperLeftCornerCoordinates() error: No DICOM file loaded" << std::endl;
	// Return a zero point
	PointType zeroPoint;
	zeroPoint.x = zeroPoint.y = zeroPoint.z = 0;
	return zeroPoint;
}
// Get the image plane upper left corner at the given slice index
dicomReader::PointType dicomReader::getImagePlaneUpperLeftCornerCoordinates(unsigned int index)
{
	if(dicomLoaded)
	{
		PointType  S;
		S.x = reader->GetOutput()->GetOrigin()[0];
		S.y = reader->GetOutput()->GetOrigin()[1];
		S.z = reader->GetOutput()->GetOrigin()[2];
		VectorType X = getRowDirectionCosine();
		VectorType Y = getColumnDirectionCosine();
		double    di = getRowSpacing();
		double    dj = getColumnSpacing();

		// Compute the point location
		PointType pointLocation;
		pointLocation.x = (-di/2.00)*X.x + (-dj/2.00)*Y.x + S.x;
		pointLocation.y = (-di/2.00)*X.y + (-dj/2.00)*Y.y + S.y;
		pointLocation.z = (-di/2.00)*X.z + (-dj/2.00)*Y.z + S.z + index*getSliceThickness()/2.00;

		return pointLocation;
	}
	else
		std::cout << "getImagePlaneUpperLeftCornerCoordinates() error: No DICOM file loaded" << std::endl;
	// Return a zero point
	PointType zeroPoint;
	zeroPoint.x = zeroPoint.y = zeroPoint.z = 0;
	return zeroPoint;
}
// Get the image plane upper left corner
dicomReader::PointType dicomReader::getImagePlaneLowerRightCornerCoordinates(unsigned int index)
{
	if(dicomLoaded)
	{
		PointType  S;
		S.x = reader->GetOutput()->GetOrigin()[0];
		S.y = reader->GetOutput()->GetOrigin()[1];
		S.z = reader->GetOutput()->GetOrigin()[2];
		VectorType X = getRowDirectionCosine();
		VectorType Y = getColumnDirectionCosine();
		double    di = getRowSpacing();
		double    dj = getColumnSpacing();
		unsigned int rowNumber = getRowNumber();
		unsigned int columnNumber = getRowNumber();

		// Compute the point location
		PointType pointLocation;
		pointLocation.x = (-di/2.00+di*rowNumber)*X.x + (-dj/2.00+dj*columnNumber)*Y.x + S.x;
		pointLocation.y = (-di/2.00+di*rowNumber)*X.y + (-dj/2.00+dj*columnNumber)*Y.y + S.y;
		pointLocation.z = (-di/2.00+di*rowNumber)*X.z + (-dj/2.00+dj*columnNumber)*Y.z + S.z + index*getSliceThickness()/2.00;

		return pointLocation;
	}
	else
		std::cout << "getImagePlaneLowerRightCornerCoordinates() error: No DICOM file loaded" << std::endl;
	// Return a zero point
	PointType zeroPoint;
	zeroPoint.x = zeroPoint.y = zeroPoint.z = 0;
	return zeroPoint;
}

dicomReader::RGBAImageType::Pointer dicomReader::getImageSlice(unsigned int sliceNumber, unsigned char imageAplha)
{
	if(dicomLoaded)
	{
		// Get the DICOM images (stored in 1x3 array)
		InputImageType::Pointer image;
		image = reader->GetOutput();

		if(sliceNumber>=0 && sliceNumber<image->GetLargestPossibleRegion().GetSize(2))
		{
			// See the example code:
			// http://www.itk.org/Doxygen/html/Examples_2IO_2ImageReadExtractFilterInsertWrite_8cxx-example.html#_a3

			ExtractFilterType::Pointer extractFilter = ExtractFilterType::New();
			extractFilter->SetDirectionCollapseToSubmatrix();

			// Get the original image size along x and y
			InputImageType::SizeType size = image->GetBufferedRegion().GetSize();
			size[2] = 0;

			// Get the start point at the given slice (in this case sagittal slice)
			// Need to be fixed for other directions maybe
			InputImageType::IndexType start = image->GetBufferedRegion().GetIndex();
			start[2] = sliceNumber;

			// Create the interest region
			InputImageType::RegionType desiredRegion;
			desiredRegion.SetSize(  size  );
			desiredRegion.SetIndex( start );

			extractFilter->SetExtractionRegion(desiredRegion);

			// Create the filter
			extractFilter->SetInput( image );
			extractFilter->Update();

			typedef itk::IntensityWindowingImageFilter< OutputImageType, OutputImageType > windowFilterType;
			windowFilterType::Pointer windowFilter = windowFilterType::New();
			windowFilter->SetInput( extractFilter->GetOutput() );
			windowFilter->SetWindowMinimum(lowerThreshold);
			windowFilter->SetWindowMaximum(upperThreshold);
			windowFilter->SetOutputMinimum(0);
			windowFilter->SetOutputMaximum(255);
			windowFilter->Update();

			typedef itk::CastImageFilter< OutputImageType, RGBAImageType > CastFilterType;
			CastFilterType::Pointer castFilter = CastFilterType::New();
			castFilter->SetInput(windowFilter->GetOutput());
			castFilter->Update();


			// Create the new image from the filter
			RGBAImageType::Pointer newImage;
			newImage = castFilter->GetOutput();

			// Iterate over the image to set alpha values
			RGBAImageType::RegionType region = newImage->GetLargestPossibleRegion();
			itk::ImageRegionIterator<RGBAImageType> imageIterator(newImage,region);
			while(!imageIterator.IsAtEnd())
			{
				RGBAPixelType pixel;
				pixel = imageIterator.Get();
				pixel.SetAlpha(imageAplha);
				imageIterator.Set(pixel);
				++imageIterator;
			}


			return newImage;

		}
	}
	else
		std::cout << "getImageSlice() error: No DICOM file loaded" << std::endl;
}

// Get modality name
std::string dicomReader::getModality()
{
	if(dicomLoaded)
	{
		// Try to get direction cosine TAG
		return readDicomTag("0008|0060");
	}
	else
		std::cout << "getModality() error: No DICOM file loaded" << std::endl;
	// Return a void string
	std::string zero;
	return zero;
}

// Get equipment name
std::string dicomReader::getManifacturerName()
{
	if(dicomLoaded)
	{
		// Try to get direction cosine TAG
		return readDicomTag("0008|0070");
	}
	else
		std::cout << "getManifacturerName() error: No DICOM file loaded" << std::endl;
	// Return a void string
	std::string zero;
	return zero;
}

// Get manifacturer name
std::string dicomReader::getEquipmentName()
{
	if(dicomLoaded)
	{
		// Try to get direction cosine TAG
		return readDicomTag("0008|1090");
	}
	else
		std::cout << "getEquipmentName() error: No DICOM file loaded" << std::endl;
	// Return a void string
	std::string zero;
	return zero;
}

// Get patient sex
std::string dicomReader::getPatientSex()
{
	if(dicomLoaded)
	{
		// Try to get direction cosine TAG
		return readDicomTag("0010|0040");
	}
	else
		std::cout << "getPatientSex() error: No DICOM file loaded" << std::endl;
	// Return a void string
	std::string zero;
	return zero;
}

// Get patient name
std::string dicomReader::getPatientAge()
{
	if(dicomLoaded)
	{
		// Try to get direction cosine TAG
		return readDicomTag("0010|1010");
	}
	else
		std::cout << "getPatientName() error: No DICOM file loaded" << std::endl;
	// Return a void string
	std::string zero;
	return zero;
}

// Set image conversion thresholds
void dicomReader::setLowerThreshold(int value)
{
	if(value>MINIMUN_ALLOWED_THRESHOLD && value<upperThreshold)
		lowerThreshold = value;
}
void dicomReader::setUpperThreshold(int value)
{
	if(value<MAXIMUM_ALLOWED_THRESHOLD && value>lowerThreshold)
		upperThreshold = value;
}

// fwrite in Big Endian mode
void dicomReader::fwriteBigEndian(const void *ptr, size_t size_of_elements, size_t number_of_elements, FILE *a_file)
{
	unsigned char *pointer = (unsigned char *) ptr;

	// Cycle over the elements
	for(unsigned int i=0;i<number_of_elements;i++)
	{
		for(int j=size_of_elements-1;j>=0;j--)
		{
			fwrite(pointer+j, 1, 1, a_file);
		}
		// Go to the next element
		pointer += size_of_elements;
	}
}

// Function used to create the 3D volume for Oliver Renderer
// The file is formatted accordin to:
//      http://idav.ucdavis.edu/~okreylos/PhDStudies/Spring2000/ECS277/index.html
// in the "Data Set File Format" section
bool dicomReader::createVolFile(char *fileN)
{
	std::string fileName;
	FILE *fp;

	// Declaring 32 and 8 bit data variable (verified before with the following lines)
	int numX, numY, numZ, zeroN;
	float xSize,ySize,zSize;
	float xOrigin,yOrigin,zOrigin;
	unsigned char *dataPtr;

	//std::cout << "Size of int: " << sizeof(int) << std::endl;
	//std::cout << "Size of float: " << sizeof(float) << std::endl;
	//std::cout << "Size of unsigned char: " << sizeof(unsigned char) << std::endl;


	if(dicomLoaded)
	{
		if(!fileN)
		{
			std::cout << "createVolFile(): No explicit volume name given: using the the DICOM directory name!" << std::endl;
			fileName.append(dicomDirName);
			if(fileName.at(fileName.length()-1)=='/')
			{
				fileName.erase(fileName.length()-1,1);
			}
			fileName.append(".vol");
			std::cout << fileName << std::endl;
		}
		else
		{
			fileName.append(fileN);
		}

		// Open the binary file overtwriting the actual copy
		fp = fopen(fileName.c_str(), "w+b");

		if(fp)
		{
			// Get the DICOM images (stored in 1x3 array)
			InputImageType::Pointer image;
			image = reader->GetOutput();

			typedef itk::IntensityWindowingImageFilter< InputImageType, InputImageType > windowFilterType;
			windowFilterType::Pointer windowFilter = windowFilterType::New();
			windowFilter->SetInput( image );
			windowFilter->SetWindowMinimum(lowerThreshold);
			windowFilter->SetWindowMaximum(upperThreshold);
			windowFilter->SetOutputMinimum(0);
			windowFilter->SetOutputMaximum(255);
			windowFilter->Update();

			typedef itk::CastImageFilter< InputImageType, VolumeRGBImageType > CastFilterType;
			CastFilterType::Pointer castFilter = CastFilterType::New();
			castFilter->SetInput(windowFilter->GetOutput());
			castFilter->Update();

			// Create the new image from the filter
			VolumeRGBImageType::Pointer newImage;
			newImage = castFilter->GetOutput();

			// Get the dimensions of the array
			numX = getColumnNumber();
			numY = getRowNumber();
			numZ = getSlicesNumber();

			// Compute the size
			xSize = (float) numX*getColumnSpacing();
			ySize = (float) numY*getRowSpacing();
			zSize = (float) numZ*getSliceThickness()/2.00;

			// Write the number of grid points on the file
			fwriteBigEndian(&numX, sizeof(numX), 1, fp);
			fwriteBigEndian(&numY, sizeof(numY), 1, fp);
			fwriteBigEndian(&numZ, sizeof(numZ), 1, fp);

			// Write the border (0 for now)
			zeroN = 0;
			fwriteBigEndian(&zeroN, sizeof(zeroN), 1, fp);

			// Compute the origin point
			PointType origin = getImagePlaneUpperLeftCornerCoordinates(0);
			xOrigin = origin.x;
			yOrigin = -origin.y - ySize;
			zOrigin = -origin.z - zSize;

			// Write the origin to the file
			fwriteBigEndian(&xOrigin, sizeof(xOrigin), 1, fp);
			fwriteBigEndian(&yOrigin, sizeof(yOrigin), 1, fp);
			fwriteBigEndian(&zOrigin, sizeof(zOrigin), 1, fp);

			// Write the sizes to the file
			fwriteBigEndian(&xSize, sizeof(xSize), 1, fp);
			fwriteBigEndian(&ySize, sizeof(ySize), 1, fp);
			fwriteBigEndian(&zSize, sizeof(zSize), 1, fp);

			// Get the buffer pointer to data
			dataPtr = (unsigned char*) newImage->GetBufferPointer();

			// Write the volume data to the file
			/*for(unsigned int k=0;k<getSlicesNumber();k++)
			{
				for(unsigned int i=0;i<numY;i++)
				{
					for(unsigned int j=0;j<numX;j++)
					{
						// Write the R value and jump the G,B values toward the next pixel triplet
						fwrite(dataPtr + k*numY*numX*3 + i*numX*3 + j*3, 1, 1, fp);
					}
				}
			}*/

			for(int k=0;k<numX;k++)
			{
				for(int i=numY-1;i>=0;i--)
				{
					for(int j=getSlicesNumber()-1;j>=0;j--)
					{
						// Write the R value and jump the G,B values toward the next pixel triplet
						fwrite(dataPtr + j*numY*numX*3 + i*numX*3 + k*3, 1, 1, fp);
					}
				}
			}

			// Close the file
			fclose(fp);
		}
		else
		{
			std::cout << "createVolFile() error: Cannot write volume file!" << std::endl;
			return false;
		}
	}
	else
	{
		std::cout << "createVolFile() error: No DICOM file loaded" << std::endl;
		return false;
	}
	// Return the true value of the function: success
	return true;
}
