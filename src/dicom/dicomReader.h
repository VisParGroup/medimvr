/*
 * dicomReader.h
 *
 *  Created on: Mar 26, 2015
 *      Author: Francesco Ricciardi
 */

#ifndef DICOMREADER_H_
#define DICOMREADER_H_

#include<iostream>
#include<string>
#include<cstdlib>

#include "itkVersion.h"

#include "itkImage.h"
#include "itkMinimumMaximumImageFilter.h"

#include "itkGDCMImageIO.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkNumericSeriesFileNames.h"

#include "itkImageSeriesReader.h"
#include "itkExtractImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRGBAPixel.h"

#include <itksys/SystemTools.hxx>


#define MINIMUN_ALLOWED_THRESHOLD -2000
#define MAXIMUM_ALLOWED_THRESHOLD +3000
#define DEFAULT_LOWER_THRESHOLD -500
#define DEFAULT_UPPER_THRESHOLD +1000


class dicomReader
{
public:
	  typedef signed short PixelType;
	  typedef itk::RGBAPixel< unsigned char > RGBAPixelType;
	  typedef itk::RGBPixel< unsigned char > RGBPixelType;

	  typedef itk::Image< PixelType, 3 >
	    InputImageType;
	  typedef itk::Image< PixelType, 2 >
	    OutputImageType;
	  typedef itk::Image< RGBPixelType, 2 >
		RGBImageType;
	  typedef itk::Image< RGBPixelType, 3 >
		VolumeRGBImageType;
	  typedef itk::Image< RGBAPixelType, 2 >
	    RGBAImageType;
	  typedef itk::Image< RGBAPixelType, 3 >
	  	VolumeRGBAImageType;
	  typedef itk::ImageSeriesReader< InputImageType >
	    ReaderType;
	  typedef itk::GDCMImageIO
	    ImageIOType;
	  typedef itk::GDCMSeriesFileNames
	    InputNamesGeneratorType;
	  typedef itk::ExtractImageFilter< InputImageType, OutputImageType >
	  	ExtractFilterType;

public:

	typedef struct point  {double x; double y; double z;} PointType;
	typedef struct vector {double x; double y; double z;} VectorType;

	dicomReader();
	dicomReader(std::string const &directoryName);
	~dicomReader();

	// Set the DICOM directory name
	void setDicomDirectory(std::string const &directoryName);

	// Trigger the directory reading and file loading
	bool readDicomDirectory();

	// Print DICOM tags
	void printDicomTags();

	// Read a specific DICOM Tags given in the format "0020|0032" to tagID
	// If valid it return a std::string containing the desired spacing
	// otherwise an empty string is returned
	std::string readDicomTag(std::string tagID);

	// Get the Row spacing in the image matrix in millimeters
	// The value 0 represent a not valid spacing (maybe absent tag)
	double getRowSpacing();
	// Get the Column spacing in the image matrix in millimeters
	// The value 0 represent a not valid spacing (maybe absent tag)
	double getColumnSpacing();
	// Get the number of row
	// The value 0 represent a not valid spacing (maybe absent tag)
	unsigned int getRowNumber();
	// Get the number of column
	// The value 0 represent a not valid spacing (maybe absent tag)
	unsigned int getColumnNumber();
	// Get the number of slice of the series
	unsigned int getSlicesNumber();
	// Get the nominal slice thickness in millimeters
	// The value 0 represent a not valid spacing (maybe absent tag)
	double getSliceThickness();
	// Get the image position i.e. the x,y,z coordinates of the upper left
	// hand corner of the image. It is the center of the first volex transmitted.
	PointType getImagePosition();
	// Get the direction cosine for the rows of the image
	VectorType getRowDirectionCosine();
	// Get the direction cosine for the coulmns of the image
	VectorType getColumnDirectionCosine();

	// Get the image position upper left corner
	PointType getImagePositionCoordinates();

	// Get the image plane upper left corner at the given slice index
	PointType getImagePlaneUpperLeftCornerCoordinates(unsigned int sliceIndex);
	// Get the image plane lower right corner at the given slice index
	PointType getImagePlaneLowerRightCornerCoordinates(unsigned int sliceIndex);

	// Get the image series lower index
	int getImageSeriesLowerIndex();
	// Get the image series upper index
	int getImageSeriesUpperIndex();

	// Get the image slice at given index number with the given alpha value (0-255)
	RGBAImageType::Pointer getImageSlice(unsigned int index, unsigned char imageAplha);

	// Get modality name
	std::string getModality();
	// Get equipment name
	std::string getManifacturerName();
	// Get manifacturer name
	std::string getEquipmentName();
	// Get patient sex
	std::string getPatientSex();
	// Get patient age
	std::string getPatientAge();

	// Set image conversion threshold
	// Only values in the range [MINIMUN_ALLOWED_THRESHOLD,MAXIMUM_ALLOWED_THRESHOLD] are permitted
	// it sets only values of the upper threshold greater than the actual lower threshold and viceversa
	void setLowerThreshold(int value);
	void setUpperThreshold(int value);

	// Function used to create the 3D volume for Oliver Renderer
	// The file is formatted accordin to:
	//      http://idav.ucdavis.edu/~okreylos/PhDStudies/Spring2000/ECS277/index.html
	// in the "Data Set File Format" section
	bool createVolFile(char *fileName = 0);

private:

	// Internal state for loading ok
	bool dicomLoaded;

	// The directory name of the file
	std::string dicomDirName;

	// Series name generator
	InputNamesGeneratorType::Pointer inputNames;
	// ITK Image IO class
	ImageIOType::Pointer gdcmIO;
	// ITK Reader
	ReaderType::Pointer reader;

	// Variable that contains DICOM conversion levels
	int lowerThreshold, upperThreshold;

	void fwriteBigEndian(const void *ptr, size_t size_of_elements, size_t number_of_elements, FILE *a_file);
};


#endif /* DICOMREADER_H_ */
