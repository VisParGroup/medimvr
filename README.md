![medimvrheader.jpg](https://bitbucket.org/repo/BpKd48/images/1442341583-medimvrheader.jpg)
# README for MedImVR
-------------------------

## Overview

MedImVR is a medical visualization tool based on VRUI [http://idav.ucdavis.edu/~okreylos/ResDev/Vrui/](http://idav.ucdavis.edu/~okreylos/ResDev/Vrui/). MedImVR is a completely portable solution. It can be used on a personal computer or in an immersive or semi-immersive environment without the needing of application changes. This is possible exploiting the great power of the VRUI platform.

The application offers the capability to load medical models saved in OBJ, PLY, LWO, LWS and ASE formats and can also load DICOM medical images from CT or MRI. This allow having a more medical friendly visualization. Another application feature is the native support of volumetric rendering, in fact volume files in .vol or NRRDI formats (NRRDI or NHDR+RAW file) can be loaded. The application has also the capability to create .vol volume files from DICOM volumes. Volume rendering can be visualized together with CT or MRI scans and medical models and many visualization parameters can be adjusted in order to have the maximum flexibility in visualization.


## Requirements

To compile MedImVR, some prerequisites must be met:

* CMake must be installed and functional
* VRUI must be installed and compiled
* ITK libraries must be installed 
* SOIL libraries

## Build MedImVR
To build the application you need to create a build directory. An example of building could be the following
```
    mkdir MedImVR-build
    cd MedImVR-build
    cmake PATH_TO_MEDIMVR_SOURCES
    make
```

Cmake could ask the user to set the ITK_DIR and VRUI_PKGCONFIG_DIR directories. ITK_DIR is the directory where ITK resides. VRUI_PKGCONFIG_DIR corresponds to the directory of VRUI build PATH_TO_VRUI_INSTALL/lib/x86_64-linux-gnu/pkgconfig.    

## Run MedImVR
To run MedImVR you need at least one of the following:

* A DICOM directory containing CT or MRI scans
* One or more medical models in OBJ, PLY, LWO, LWS or ASE format

Some free DICOM datasets can be freely downloaded from internet. In all the following examples we assume that the files or directories are under the MedImVR build directory. If you run MedImVR without arguments a usage guide will be printed.

### Example 1
Load the "bone.obj" model file from the "models" subdirectory together with CT scans in the "DICOM" subdirectory.
```    
    ./MedImVR ./models/bone.obj -dd ./DICOM
```

### Example 2
Load all model files in the "models" subdirectory together with CT scans in the "DICOM" subdirectory.

```       
    ./MedImVR -d ./models -dd ./DICOM
```    

### Example 3
Create the "myVolume.vol" volumetric rendering in .vol format of the CT scans in the "DICOM" subdirectory and load it in the application together with the DICOM images.

```    
    ./MedImVR -dd ./DICOM -cv myVolume -v myVolume.vol
```    

##Citation
If you use this software for your research, please cite the following paper:
Francesco Ricciardi, Emiliano Pastorelli, Lucio Tommaso De Paolis, and Heiko Herrmann. Scalable medical viewer for virtual reality environments. In Lucio Tommaso De Paolis and Antoni Mongelli, editors, Augmented and Virtual Reality, volume 9254 of Lecture Notes in Computer Science, pages 233-243. Springer International Publishing, 2015. DOI: [10.1007/978-3-319-22888-4_17](http://dx.doi.org/10.1007/978-3-319-22888-4_17)

## Disclaimer.
The MedImVR software, and the contents of the MedImVR documentation, are intended for educational, research, and informational purposes only.
MedImVR, or information derived from MedImVR, may be used only for these purposes and may not under any circumstances whatsoever be used for clinical purposes.
The MedImVR software has not been approved by the FDA and is not intended for treating or diagnosing human subjects, and the recipient and user will not use the MedImVR software for such purposes.
The MedImVR copyright holders and contributors and all affiliated organizations shall not be liable for any damages arising out of the use of MedImVR by any party for any purpose.